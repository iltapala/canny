#include "shared.h"
#include "timing.h"
#include "rock.h"

namespace rock {

    uint8_t grayscale(const RGB& c)
    {
        // https://rosettacode.org/wiki/Grayscale_image
        //return static_cast<uint8_t>(0.2126f * c.r + 0.7152f * c.g + 0.0722 * c.b);

        //https://www.mathworks.com/help/matlab/ref/rgb2gray.html
        //return static_cast<uint8_t>(0.2989f * c.r + 0.5870f * c.g + 0.1140f * c.b);
        return static_cast<uint8_t>((2989 * c.r + 5870 * c.g + 1140 * c.b) / 10000);
    }

    void convertToGrayScale(const Image& input, ImageBuffer<uint8_t>& output)
    {
        for (size_t y = 0; y < input.getHeight(); ++y) {
            for (size_t x = 0; x < input.getWidth(); ++x) {
                output.set(x, y, grayscale(input.getRGB(x, y)));
            }
        }
    }

    static const SeparableKernel<5> gaussianKernel5(
        5607424,
        { 261, 561, 724, 561, 261 },
        { 261, 561, 724, 561, 261 }
    );

    template <typename T>
    void gaussianBlur5x5(const ImageBuffer<T>& input, ImageBuffer<T>& output)
    {
        gaussianKernel5.convolute(input, output);
    }

    template <typename T>
    void gaussianBlur5x5Inplace(ImageBuffer<T>& inputOutput)
    {
        gaussianKernel5.convolute(inputOutput, inputOutput);
    }

    static const SeparableKernel<3> sobelXKernel(
        1,
        { 1, 0, -1 },
        { 1, 2, 1 }
    );

    static const SeparableKernel<3> sobelYKernel(
        1,
        { 1, 2, 1 },
        { 1, 0, -1 }
    );

    void calculateSobel(const ImageBuffer<uint8_t>& input, 
        ImageBuffer<int16_t>& outputX, ImageBuffer<int16_t>& outputY)
    {
        if (input.getWidth() < 4) {
            sobelXKernel.convolute(input, outputX);
            sobelYKernel.convolute(input, outputY);
            return;
        }

        assert((input.getWidth() == outputX.getWidth()) && (input.getHeight() == outputX.getHeight()));
        assert((input.getWidth() == outputY.getWidth()) && (input.getHeight() == outputY.getHeight()));

        Window3x3 window(input);
        for (int y = 0; y < input.getHeight(); ++y) {
            for (int x = 0; ; x++) {
                const auto a = window.a();
                const auto b = window.b();
                const auto c = window.c();

                // Calculate sobel filter values
                const auto ax = a & 0xff00ff;
                const auto bx = (b & 0xff00ff) << 1;
                const auto cx = c & 0xff00ff;
                const auto sx = ax + bx + cx;
                int16_t sumX = static_cast<int16_t>(sx & 0xffff) - static_cast<int16_t>(sx >> 16);
                outputX.set(x, y, sumX);

                const auto ay = (a & 0xff) + ((a & 0xff00) >> 7) + (a >> 16);
                const auto cy = (c & 0xff) + ((c & 0xff00) >> 7) + (c >> 16);
                int16_t sumY = static_cast<int16_t>(ay) - static_cast<int16_t>(cy);
                outputY.set(x, y, sumY);

                if (!window.moveNext()) {
                    assert(x + 1 == input.getWidth());
                    break;
                }
            }
        }
    }

    void calculateGradient(const ImageBuffer<uint8_t>& input, ImageBuffer<int16_t>& Gx, ImageBuffer<int16_t>& Gy,
        ImageBuffer<int>& G)
    {
        // Make sobel convolutions
        calculateSobel(input, Gx, Gy);

        // Calculate magnitude
        const auto* dx = Gx.getRawData();
        const auto* dy = Gy.getRawData();
        auto* mag = G.getRawData();
        for (size_t i = 0; i < input.getPixelCount(); ++i) {
            mag[i] = dx[i] * dx[i] + dy[i] * dy[i];
        }
    }

    void nonMaxSuppressionAndThreshold(
        const ImageBuffer<int>& G, const ImageBuffer<int16_t>& Gx, const ImageBuffer<int16_t>& Gy,
        ImageBuffer<uint8_t>& output, int Gmax, float lowThresholdRatio, float highThresholdRatio,
        std::stack<std::pair<int, int>>& strong)
    {
        const auto maximum = std::min(Gmax, 255);
        auto highThreshold = static_cast<int>(maximum * highThresholdRatio);
        auto lowThreshold = static_cast<int>(highThreshold * lowThresholdRatio);

        // G is magnitude so raise thresholds to power of two
        highThreshold *= highThreshold;
        lowThreshold *= lowThreshold;

        for (size_t y = 0; y < G.getHeight(); ++y) {
            for (size_t x = 0; x < G.getWidth(); ++x) {
                auto xs = Gx.get(x, y);
                auto ys = Gy.get(x, y);
                int ax = (int)std::abs(xs);
                int ay = (int)std::abs(ys) << 15;

                const int TG22 = 13573;
                int tg22x = ax * TG22;

                int q, r;
                if (ay < tg22x)
                {
                    q = G.safeGet(x + 1, y);
                    r = G.safeGet(x - 1, y);
                }
                else
                {
                    int tg67x = tg22x + (ax << 16);
                    if (ay > tg67x)
                    {
                        q = G.safeGet(x, y + 1);
                        r = G.safeGet(x, y - 1);
                    }
                    else
                    {
                        if ((xs ^ ys) < 0) {
                            q = G.safeGet(x - 1, y + 1);
                            r = G.safeGet(x + 1, y - 1);
                        }
                        else {
                            q = G.safeGet(x + 1, y + 1);
                            r = G.safeGet(x - 1, y - 1);
                        }
                    }
                }

                const auto v = G.get(x, y);
                uint8_t c = 0;
                if (((v >= q) && (v >= r))) {
                    if (v >= highThreshold) {
                        c = STRONG;
                        strong.push(std::make_pair(static_cast<int>(x), static_cast<int>(y)));
                    }
                    else if (v >= lowThreshold) {
                        c = WEAK;
                    }
                }

                output.set(x, y, c);
            }
        }
    }

    void nonMaxSuppressionAndThreshold2_Line(
        const int* prev, const int* current, const int* next, 
        const int16_t* gx, const int16_t* gy, uint8_t* output,
        int w, int y, int highThreshold, int lowThreshold, std::stack<std::pair<int, int>>& strong)
    {
        constexpr int TG22 = 13573;
        size_t x = 0;
        {
            auto xs = *gx++;
            auto ys = *gy++;
            int ax = (int)std::abs(xs);
            int ay = (int)std::abs(ys) << 15;

            int tg22x = ax * TG22;

            int q, r;
            if (ay < tg22x)
            {
                q = current[+1];
                r = current[0];
            }
            else
            {
                int tg67x = tg22x + (ax << 16);
                if (ay > tg67x)
                {
                    q = *next;
                    r = *prev;
                }
                else
                {
                    if ((xs ^ ys) < 0) {
                        q = next[0];
                        r = prev[+1];
                    }
                    else {
                        q = next[+1];
                        r = prev[0];
                    }
                }
            }

            const auto v = *current;
            uint8_t c = 0;
            if (((v >= q) && (v >= r))) {
                if (v >= highThreshold) {
                    c = STRONG;
                    strong.push(std::make_pair(static_cast<int>(x), static_cast<int>(y)));
                }
                else if (v >= lowThreshold) {
                    c = WEAK;
                }
            }

            *output++ = c;
            prev++;
            current++;
            next++;
        }

        for (; x < w - 1; ++x) {
            auto xs = *gx++;
            auto ys = *gy++;
            int ax = (int)std::abs(xs);
            int ay = (int)std::abs(ys) << 15;

            int tg22x = ax * TG22;

            int q, r;
            if (ay < tg22x)
            {
                q = current[+1];
                r = current[-1];
            }
            else
            {
                int tg67x = tg22x + (ax << 16);
                if (ay > tg67x)
                {
                    q = *next;
                    r = *prev;
                }
                else
                {
                    if ((xs ^ ys) < 0) {
                        q = next[-1];
                        r = prev[+1];
                    }
                    else {
                        q = next[+1];
                        r = prev[-1];
                    }
                }
            }

            const auto v = *current;
            uint8_t c = 0;
            if (((v >= q) && (v >= r))) {
                if (v >= highThreshold) {
                    c = STRONG;
                    strong.push(std::make_pair(static_cast<int>(x), static_cast<int>(y)));
                }
                else if (v >= lowThreshold) {
                    c = WEAK;
                }
            }

            *output++ = c;
            prev++;
            current++;
            next++;
        }

        {
            auto xs = *gx;
            auto ys = *gy;
            int ax = (int)std::abs(xs);
            int ay = (int)std::abs(ys) << 15;

            int tg22x = ax * TG22;

            int q, r;
            if (ay < tg22x)
            {
                q = current[0];
                r = current[-1];
            }
            else
            {
                int tg67x = tg22x + (ax << 16);
                if (ay > tg67x)
                {
                    q = *next;
                    r = *prev;
                }
                else
                {
                    if ((xs ^ ys) < 0) {
                        q = next[-1];
                        r = prev[0];
                    }
                    else {
                        q = next[0];
                        r = prev[-1];
                    }
                }
            }

            const auto v = *current;
            uint8_t c = 0;
            if (((v >= q) && (v >= r))) {
                if (v >= highThreshold) {
                    c = STRONG;
                    strong.push(std::make_pair(static_cast<int>(x), static_cast<int>(y)));
                }
                else if (v >= lowThreshold) {
                    c = WEAK;
                }
            }

            *output = c;
        }
    }

    void nonMaxSuppressionAndThreshold2(
        const ImageBuffer<int>& G, const ImageBuffer<int16_t>& Gx, const ImageBuffer<int16_t>& Gy,
        ImageBuffer<uint8_t>& out, int Gmax, float lowThresholdRatio, float highThresholdRatio,
        std::stack<std::pair<int, int>>& strong)
    {
        const auto maximum = std::min(Gmax, 255);
        auto highThreshold = static_cast<int>(maximum * highThresholdRatio);
        auto lowThreshold = static_cast<int>(highThreshold * lowThresholdRatio);

        // G is magnitude so raise thresholds to power of two
        highThreshold *= highThreshold;
        lowThreshold *= lowThreshold;

        const auto w = G.getWidth();
        const auto h = G.getHeight();
        const auto* prevLine = G.getRawData();
        const auto* currentLine = prevLine;
        const auto* gx = Gx.getRawData();
        const auto* gy = Gy.getRawData();
        auto* output = out.getRawData();

        size_t y = 0;
        for (y = 0; y < h - 1; ++y) {
            const auto* nextLine = currentLine + w;
            nonMaxSuppressionAndThreshold2_Line(
                prevLine, currentLine, nextLine, gx, gy, output, w, y, highThreshold, lowThreshold, strong);
            output += w;
            gx += w;
            gy += w;
            prevLine = currentLine;
            currentLine = nextLine;
        }

        nonMaxSuppressionAndThreshold2_Line(
            prevLine, currentLine, currentLine, gx, gy, output, w, y, highThreshold, lowThreshold, strong);
    }

    void hysteresis(ImageBuffer<uint8_t>& input, std::stack<std::pair<int, int>>& stack)
    {
        const int maxX = input.getWidth() - 1;
        const int maxY = input.getHeight() - 1;

        // Color weak edges next to strong edges
        while (!stack.empty())
        {
            const auto p = stack.top();
            stack.pop();

            const auto x = p.first;
            const auto y = p.second;

            if (x > 0) {
                if ((y > 0) && (input.get(x - 1, y - 1) == WEAK)) { input.set(x - 1, y - 1, STRONG); stack.push(std::make_pair(x - 1, y - 1)); }
                if (input.get(x - 1, y) == WEAK) { input.set(x - 1, y, STRONG); stack.push(std::make_pair(x - 1, y)); }
                if ((y < maxY) && (input.get(x - 1, y + 1) == WEAK)) { input.set(x - 1, y + 1, STRONG); stack.push(std::make_pair(x - 1, y + 1)); }
            }
            if ((y > 0) && (input.get(x, y - 1) == WEAK)) { input.set(x, y - 1, STRONG); stack.push(std::make_pair(x, y - 1)); }
            if ((y < maxY) && (input.get(x, y + 1) == WEAK)) { input.set(x, y + 1, STRONG); stack.push(std::make_pair(x, y + 1)); }
            if (x < maxX) {
                if ((y > 0) && (input.get(x + 1, y - 1) == WEAK)) { input.set(x + 1, y - 1, STRONG); stack.push(std::make_pair(x + 1, y - 1)); }
                if (input.get(x + 1, y) == WEAK) { input.set(x + 1, y, STRONG); stack.push(std::make_pair(x + 1, y)); }
                if ((y < maxY) && (input.get(x + 1, y + 1) == WEAK)) { input.set(x + 1, y + 1, STRONG); stack.push(std::make_pair(x + 1, y + 1)); }
            }
        }
    }

    void removeWeak(ImageBuffer<uint8_t>& input)
    {
        // Remove rest of the weak edges
        auto* data = input.getRawData();
        for (size_t i = 0; i < input.getPixelCount(); ++i) {
            if (data[i] == WEAK) {
                data[i] = 0;
            }
        }
    }
}

using namespace rock;

void executeRock(const Image& original, ImageBuffer<uint8_t>& result)
{
    double t1 = get_elapsed_mcycles();
    ImageBuffer<uint8_t> data(original.getWidth(), original.getHeight());
    convertToGrayScale(original, data);
    double t2 = get_elapsed_mcycles();
    b12 = std::min(b12, t2 - t1);

    // TODO: Gaussian blur in place to -2,-2 offset
    gaussianBlur5x5Inplace(data);
    double t3 = get_elapsed_mcycles();
    b23 = std::min(b23, t3 - t2);

    ImageBuffer<int> G(data.getWidth(), data.getHeight());
    ImageBuffer<int16_t> Gx(data.getWidth(), data.getHeight());
    ImageBuffer<int16_t> Gy(data.getWidth(), data.getHeight());
    calculateGradient(data, Gx, Gy, G);
    double t4 = get_elapsed_mcycles();
    b34 = std::min(b34, t4 - t3);

    std::stack<std::pair<int, int>> strong;
    nonMaxSuppressionAndThreshold2(
        G, Gx, Gy, result, G.max(), 0.3f, 0.275f, strong);
    double t5 = get_elapsed_mcycles();
    b45 = std::min(b45, t5 - t4);

    hysteresis(result, strong);
    removeWeak(result);
    double t6 = get_elapsed_mcycles();
    b56 = std::min(b56, t6 - t5);
}

// Integer gaussian
// Strong pushed in threshold
// Combined non-max supression and threshold steps
// Use gradient magnitude
// Custom SobelX implementation
// Bithack trick from openCV to avoid trigonometry in non-max suppression quadrant selection
// Combined custom SobelXY implementation
// Integer grayscale
// Separated gaussian kernel
// Unrolled convolution to avoid safeGet
// Removed safeGets from hysteresis
// Changed weak clearing to use one dimensional loop
// Changed gradient magnitude calculation to use one dimensional loop
