#pragma once

#ifdef _MSC_VER
#define NOMINMAX
#endif

#include <algorithm>
#include <cassert>
#include <cmath>
#include <stack>
#include <vector>
#include <sys/types.h>

#ifdef M_PI
#undef M_PI
#endif
#define M_PI 3.1415926535f

struct RGB
{
    uint8_t r, g, b;
};

struct BGR
{
    uint8_t b, g, r;
};

struct RGBQuad
{
    uint8_t r, g, b, reserved;
};

template <typename T>
class ImageBuffer
{
public:
    ImageBuffer(size_t width, size_t height)
    {
        setWidth(width);
        setHeight(height);
        createBuffer();
    }

    inline size_t getWidth() const { return m_width; }
    inline size_t getHeight() const { return m_height; }
    inline size_t getPixelCount() const
    {
        return getWidth() * getHeight();
    }

    inline size_t getBufferSize() const
    {
        return m_data.size();
    }

    inline std::vector<T>& getData()
    {
        return m_data;
    }

    inline const std::vector<T>& getData() const
    {
        return m_data;
    }

    inline T* getRawData()
    {
        return &m_data[0];
    }

    inline const T* getRawData() const
    {
        return &m_data[0];
    }

    inline T get(int x, int y) const {
        return m_data[m_width * y + x];
    }

    inline T safeGet(int x, int y) const {
        //if ((x < 0) || (y < 0) || (x >= m_width) || (y >= m_height)) return 0;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (x >= m_width) x = m_width - 1;
        if (y >= m_height) y = m_height - 1;
        return m_data[m_width * y + x];
    }

    inline T safeGetZ(int x, int y) const {
        if ((x < 0) || (y < 0) || (x >= m_width) || (y >= m_height)) return 0;
        return m_data[m_width * y + x];
    }

    inline void set(size_t x, size_t y, T value) {
        m_data[m_width * y + x] = value;
    }

    inline void clear() {
        createBuffer();
    }

    inline T max() const {
        T maximum = std::numeric_limits<T>::min();
        for (size_t y = 0; y < getHeight(); ++y) {
            for (size_t x = 0; x < getWidth(); ++x) {
                const auto value = get(x, y);
                maximum = std::max(maximum, value);
            }
        }

        return maximum;
    }

protected:
    ImageBuffer()
    {
    }

    inline void setWidth(size_t width) { m_width = width; }
    inline void setHeight(size_t height) { m_height = height; }
    inline void createBuffer()
    {
        m_data = std::vector<T>(getWidth() * getHeight());
    }

    size_t m_width, m_height;
    std::vector<T> m_data;
};

class Image : private ImageBuffer<uint8_t>
{
public:
    Image()
    {
        setWidth(0);
        setHeight(0);
        setBitsPerPixel(0);
    }

    Image(size_t width, size_t height, size_t bitsPerPixel)
    {
        setWidth(width);
        setHeight(height);
        setBitsPerPixel(bitsPerPixel);
        createBuffer();
    }

    using ImageBuffer<uint8_t>::getWidth;
    using ImageBuffer<uint8_t>::getHeight;
    using ImageBuffer<uint8_t>::getPixelCount;
    using ImageBuffer<uint8_t>::getBufferSize;
    using ImageBuffer<uint8_t>::getData;
    using ImageBuffer<uint8_t>::getRawData;
    using ImageBuffer<uint8_t>::clear;

    inline bool empty() const { return getWidth() == 0 || getHeight() == 0; }

    inline size_t getBitsPerPixel() const { return m_bitsPerPixel; }
    
    inline ImageBuffer<uint8_t>& getBuffer()
    {
        assert(m_bitsPerPixel == 8);
        return *this;
    }

    inline const ImageBuffer<uint8_t>& getBuffer() const
    {
        assert(m_bitsPerPixel == 8);
        return *this;
    }

    inline RGB getRGB(size_t x, size_t y) const {
        switch (m_bitsPerPixel)
        {
        case 24: {
            const uint8_t* p = &m_data[(m_width * y + x) * 3];
            return { p[2], p[1], p[0] };
        }
        case 32: {
            const uint8_t* p = &m_data[(m_width * y + x) * 4];
            return { p[2], p[1], p[0] };
        }
        default:
            assert(false);
            return {};
        }
    }

protected:
    inline void setBitsPerPixel(size_t bitsPerPixel) { m_bitsPerPixel = bitsPerPixel; }
    inline void createBuffer()
    {
        m_data = std::vector<uint8_t>(getWidth() * getHeight() * (getBitsPerPixel() / 8));
    }

protected:
    size_t m_bitsPerPixel;
};

Image loadTestImage(const char* name);
void saveImage(const char* name, const Image& image);
bool verifyImage(const Image& reference, const Image& other, float allowedDifference = 0.0f);

template <int N>
class SeparableKernel
{
public:
    static constexpr int MAX = N / 2;
    static constexpr int MIN = -MAX;

    SeparableKernel(int denominator,
        const std::initializer_list<int>& mx,
        const std::initializer_list<int>& my)
        : m_denominator(denominator)
    {
        assert(mx.size() == N);
        assert(my.size() == N);
        for (size_t i = 0; i < N; ++i) {
            m_dataX[i] = *(mx.begin() + i);
            m_dataY[i] = *(my.begin() + i);
        }
    }

    template <typename A, typename B>
    void convolute(const ImageBuffer<A>& input, ImageBuffer<B>& output) const
    {
        assert((input.getWidth() == output.getWidth()) && (input.getHeight() == output.getHeight()));
        ImageBuffer<int> temp(input.getWidth(), input.getHeight());

        const int endsX[] = { OFFSET, static_cast<int>(input.getWidth()) - OFFSET, static_cast<int>(input.getWidth()) };
        const int endsY[] = { OFFSET, static_cast<int>(input.getHeight()) - OFFSET, static_cast<int>(input.getHeight()) };

        int y = 0;
        for (int sy = 0; sy < 3; ++sy) {
            const auto endY = endsY[sy];
            for (; y < endY; ++y) {
                int x = 0;
                for (int sx = 0; sx < 3; ++sx) {
                    const auto endX = endsX[sx];
                    if ((x == 1) && (y == 1)) {
                        for (; x < endX; ++x) {
                            int sum = 0;
                            for (int kx = MIN; kx <= MAX; ++kx) {
                                sum += m_dataX[kx + OFFSET] * input.get(x + kx, y);
                            }

                            temp.set(x, y, sum);
                        }
                    }
                    else {
                        for (; x < endX; ++x) {
                            int sum = 0;
                            for (int kx = MIN; kx <= MAX; ++kx) {
                                sum += m_dataX[kx + OFFSET] * input.safeGet(x + kx, y);
                            }

                            temp.set(x, y, sum);
                        }
                    }
                }
            }
        }

        y = 0;
        for (int sy = 0; sy < 3; ++sy) {
            const auto endY = endsY[sy];
            for (; y < endY; ++y) {
                int x = 0;
                for (int sx = 0; sx < 3; ++sx) {
                    const auto endX = endsX[sx];
                    if ((x == 1) && (y == 1)) {
                        for (; x < endX; ++x) {
                            int sum = 0;
                            for (int ky = MIN; ky <= MAX; ++ky) {
                                sum += m_dataY[ky + OFFSET] * temp.get(x, y + ky);
                            }

                            output.set(x, y, static_cast<B>((sum + (m_denominator / 2)) / m_denominator));
                        }
                    }
                    else {
                        for (; x < endX; ++x) {
                            int sum = 0;
                            for (int ky = MIN; ky <= MAX; ++ky) {
                                sum += m_dataY[ky + OFFSET] * temp.safeGet(x, y + ky);
                            }

                            output.set(x, y, static_cast<B>((sum + (m_denominator / 2)) / m_denominator));
                        }
                    }
                }
            }
        }
    }

    template <typename A, typename B>
    B convolute(const ImageBuffer<A>& input, int x, int y) const
    {
        int sum = 0;
        for (int ky = MIN; ky <= MAX; ++ky) {
            int temp = 0;
            for (int kx = MIN; kx <= MAX; ++kx) {
                temp += m_dataX[kx + OFFSET] * input.safeGet(x + kx, y + ky);
            }

            sum += m_dataY[ky + OFFSET] * temp;
        }

        return static_cast<B>((sum + (m_denominator / 2)) / m_denominator);
    }

private:
    static constexpr int OFFSET = N / 2;
    int m_denominator;
    int m_dataX[N], m_dataY[N];
};

class Window3x3
{
public:
    Window3x3(const ImageBuffer<uint8_t>& input)
        : m_input(input)
    {
        assert(input.getWidth() >= 2);

        m_pline = m_cline = input.getRawData();
        m_nline = m_cline + input.getWidth();
        m_y = 0;
        m_x = 0;

        initLine();
    }

    void moveToNextLine()
    {
        m_pline = m_cline;
        m_cline = m_nline;
        if (++m_y < m_input.getHeight() - 1) m_nline += m_input.getWidth();

        initLine();
    }

    void initLine()
    {
        // Prepate three pixels per row, where first is duplicated
        m_a = *reinterpret_cast<const uint16_t*>(m_pline);
        m_b = *reinterpret_cast<const uint16_t*>(m_cline);
        m_c = *reinterpret_cast<const uint16_t*>(m_nline);
        m_a = (m_a & 0xff) | (m_a << 8);
        m_b = (m_b & 0xff) | (m_b << 8);
        m_c = (m_c & 0xff) | (m_c << 8);
        m_x = 0;
    }

    bool moveNext()
    {
        // Advance
        m_a >>= 8;
        m_b >>= 8;
        m_c >>= 8;
        m_x++;

        if (m_x == m_input.getWidth()) {
            moveToNextLine();
            return false;
        }

        // Get new data
        const auto nx = std::min(m_x + 1, static_cast<int>(m_input.getWidth() - 1));
        m_a |= m_pline[nx] << 16;
        m_b |= m_cline[nx] << 16;
        m_c |= m_nline[nx] << 16;
        return true;
    }

    uint32_t a() const { return m_a; }
    uint32_t b() const { return m_b; }
    uint32_t c() const { return m_c; }

private:
    const ImageBuffer<uint8_t>& m_input;
    const uint8_t *m_pline, *m_cline, *m_nline;
    uint32_t m_a, m_b, m_c;
    int m_x, m_y;
};

void executeWood(const Image& original, ImageBuffer<uint8_t>& result, bool saveIntermediateSteps);
void executeRock(const Image& original, ImageBuffer<uint8_t>& result);
void executeIron(const Image& original, ImageBuffer<uint8_t>& result);
void executeDiamond(const Image& original, ImageBuffer<uint8_t>& result);
void executePaper(const Image& original, ImageBuffer<uint8_t>& result);

extern double a12, a23, a34, a45, a56;
extern double b12, b23, b34, b45, b56;
extern double c12, c23, c34, c45, c56;
extern double d12, d23, d34, d45, d56;
extern double e12, e23, e34, e45, e56;
