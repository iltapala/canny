# Canny edge detection algorithm optimization with ISPC (Innovation Days 18.-20.12.2019)

## Background

During innovation days my goal was to learn about ISPC (Intel SIMD Program Compiler). Compiler enables a novel new way of writing code that benefits from vectorization i.e. use of available SIMD instruction sets. ISPC promises both shorter development times and easy maintainability as well as top grade performance.

To test these promises and to also learn some more about image processing, I chose Canny edge detection algorithm for investigation. I chose this algorithm, because it is widely known, it is not trivial but comprises of many simple steps. Algorithm is outlined below:

1. List item
1. Grayscale conversion
1. Gaussian blur
   - Noise reduction improves results
1. Intensity gradient calculation
   - Using Sobel operator calculates gradients in X and Y direction
   - This can be used to calculate gradient strength and angle
1. Non-maximum suppression
   - Based on gradient data thins out the edges
1. Double thresholding
   - Classifies edge pixels to weak and strong based on minimum and maximum thresholds
1. Edge hysteresis and finalization
   - Colors all weak edges to strong, if connected to a strong edge
   - Remove all left-over weak edges

# Implementation

First implementation of the algorithm I made based on [Wikipedia article](https://en.wikipedia.org/wiki/Canny_edge_detector) and [Matlab](http://justin-liang.com/tutorials/canny/) and [Python tutorial](https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123) implementations of the algorithm. Purpose of this was to verify algorithm implementation and therefore I only followed the examples and did not try to optimize implementation at this point. In retrospect I think that this was a good idea and using multiple different sources for verification paid off, because for example the Python implementation had completely broken edge hysteresis.

Goal of the second implementation was to optimize the original algorithm as much as possible using still C++ and still produce almost bit perfect results with my original algorithm implementation. Most notable algorithm optimizations:

- Integer arithmetic implementation of grayscale and gaussian steps
- Combined non-max suppression and thresholding steps
- Avoid square root by using intensity magnitude in thresholding
- Integer quadrant calculus in non-maximum suppression (courtesy of OpenCV), which enables removal use of trigonometry functions
- Split 5x5 gaussian kernel to 1x5 and 5x1 kernels
- Optimized sobel operator memory accesses with 3x3 window
- Unrolled kernel convolution, non-maximum supressions and hysteresis loops to avoid per value boundary checks

For the third implementation I used three box blur passes to approximate gaussian blur. This reduces implementation accuracy but is faster. 

Fourth implementation I made with ISPC and fifth with SSE2 (and SSE4.1) compiler intrinsics. For both of the implementations I had to change few algorithms to better suit for vectorization. Stuff like accumulators (running sum) work well for general purpose code, but for vectorization calculating from sums from scratch is usually faster for small kernels.

## Learnings from ISPC

After getting used to ISPC concepts and syntax, I found it quite easy to write algorithms with ISPC. ISPC syntax is like C with some additional keywords, so porting existing general purpose algorithms to ISPC is easy. Therefore, I totally buy Intel's promises for faster development times compared to handwritten vectorized code. Also maintaining and modifying existing ISPC code should be easy. But lack of debugger support makes bug hunting more difficult and only option is to debug on assembly level.

I found also nice that ISPC can give performance warnings about generated code. For example warning is issued when compiler has to use slow scatter or gather instructions. Compiler does not give many hints how to fix things but having at least those warnings is a plus.

ISPC works nicely for algorithms that can read and write their data in SIMD width size of blocks. It struggled with 24-bit color input in grayscale conversion failing to produce significantly faster code than optimized general purpose code. And like mentioned earlier the algorithm must suit for vectorized code. ISPC cannot find all the patterns from algorithm designed for general purpose processors and might generate even slower code than the original.

Speed of ISPC generated code is good. Better than general purpose code, but still it fails to match speed of handwritten code. In my experiments handwritten SSE2/4.1 code was faster by a margin than ISPC generated AVX & AVX2 instruction set code.

## Test results

Tests were done by running algorithms for 24-bit 480x480 picture of Lena 1000 times. Results are minimum number of instructions (millions of instructions) measured by RDTSC counter for each step (fastest of the 1000).

### Intel Core i7 8750H @ 4.1GHz (Coffee Lake, SSE4.2, AVX, AVX2)

|  | #1 Original |  #2 C++ |  #3 C++ approximations | #4 ISPC | #5 Handwritten |
|--|--|--|--|--|--|
| Grayscale             |  1.6 | 1.3 | 0.6 | 0.5 | 0.3 |
| Gaussian              | 26.7 | 6.6 | 4.2 | 1.2 | 0.5 |
| Gradient              | 26.7 | 2.5 | 2.7 | 0.4 | 0.5 |
| NonMaxSup & Threshold | 10.0 | 4.9 | 4.7 | 4.5 | 4.5 |
| Hysteresis            |  3.3 | 1.2 | 1.1 | 1.0 | 0.6 |
| **TOTAL** | **68.3 (1.0x)** | **16.5 (4.1x)** | **13.3 (5.1x)** | **7.6 (9.0x)** | **6.4 (10.7x)** |

### Intel Core i7 7800X @ 4.0GHz (Skylake-X, SSE4.2, AVX, AVX2, AVX512F)

|  | #1 Original |  #2 C++ |  #3 C++ approximations | #4 ISPC | #5 Handwritten |
|--|--|--|--|--|--|
| Grayscale             |  2.6 |  2.1 | 0.9 | 0.8 | 0.4 |
| Gaussian              | 42.8 | 10.4 | 6.5 | 0.9 | 0.7 |
| Gradient              | 42.1 |  3.9 | 4.3 | 0.8 | 0.9 |
| NonMaxSup & Threshold | 16.0 |  7.7 | 7.3 | 7.2 | 7.0 |
| Hysteresis            |  5.1 |  1.9 | 1.6 | 1.4 | 0.9 |
| **TOTAL** | **108.6 (1.0x)** | **26.0 (4.2x)** | **20.6 (5.3x)** | **11.1 (9.8x)** | **9.9 (11.0x)** |

## Future work 

Do SIMD optimization for non-max supression step. It is now clearly most time consuming but was not optimized that much, because time constrains. Current implementation has lots of conditionals and it uses std::stack, so its vectorization is would be quite difficult.

Make handwritten version that utilizes AVX instructions.

Make yet another vectorization library or tool to simplify fast code writing. Current vectorization libraries that I have tested and ISPC in most cases fail to utilize byte swizzling and other similar tricks.