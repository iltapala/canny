#pragma once

#include "shared.h"

namespace rock {
    void convertToGrayScale(const Image& input, ImageBuffer<uint8_t>& output);

    void calculateSobel(const ImageBuffer<uint8_t>& input, 
        ImageBuffer<int16_t>& outputX, ImageBuffer<int16_t>& outputY);

    void calculateGradient(const ImageBuffer<uint8_t>& input, ImageBuffer<int16_t>& Gx, ImageBuffer<int16_t>& Gy,
        ImageBuffer<int>& G);

    constexpr uint8_t STRONG = 255;
    constexpr uint8_t WEAK = 144;

    void nonMaxSuppressionAndThreshold(
        const ImageBuffer<int>& G, const ImageBuffer<int16_t>& Gx, const ImageBuffer<int16_t>& Gy,
        ImageBuffer<uint8_t>& output, int Gmax, float lowThresholdRatio, float highThresholdRatio,
        std::stack<std::pair<int, int>>& strong);
    void nonMaxSuppressionAndThreshold2(
        const ImageBuffer<int>& G, const ImageBuffer<int16_t>& Gx, const ImageBuffer<int16_t>& Gy,
        ImageBuffer<uint8_t>& output, int Gmax, float lowThresholdRatio, float highThresholdRatio,
        std::stack<std::pair<int, int>>& strong);

    void hysteresis(ImageBuffer<uint8_t>& input, std::stack<std::pair<int, int>>& stack);
    void removeWeak(ImageBuffer<uint8_t>& input);
}
