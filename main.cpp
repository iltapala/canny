#include "shared.h"
#include "timing.h"

int main(int argc, char **argv) {
    int testIterations = 1;
    if ((argc > 1) && (argc != 2)) {
        printf("%s\n", argv[0]);
        printf("Usage: canny [iterations]\n");
        getchar();
        exit(-1);
    }
    else {
        if (argc == 2) {
            testIterations = atoi(argv[1]);
        }
    }

    const auto original = loadTestImage("data\\original.bmp");
    if (original.empty()) {
        return EXIT_FAILURE;
    }

    Image reference(original.getWidth(), original.getHeight(), 8);
    executeWood(original, reference.getBuffer(), true);
    saveImage("data\\reference.bmp", reference);

    //
    // Run reference now without saving intermediate files, testIterations times, and report the minimum
    // time for any of them.
    //
    double minTimeWood = 1e30;
    Image result(original.getWidth(), original.getHeight(), 8);
    for (unsigned int i = 0; i < testIterations; i++) {
        result.clear();
        reset_and_start_timer();
        executeWood(original, result.getBuffer(), false);
        double t = get_elapsed_mcycles();
        printf("@time of WOOD run:\t\t\t[%.3f] million cycles\n", t);
        minTimeWood = std::min(minTimeWood, t);
    }

    if (!verifyImage(reference, result)) {
        printf("ERROR: WOOD verification failed!\n");
    }
    saveImage("data\\result_wood.bmp", result);

    //
    // Run reference now without saving intermediate files, testIterations times, and report the minimum
    // time for any of them.
    //
    double minTimeRock = 1e30;
    for (unsigned int i = 0; i < testIterations; i++) {
        result.clear();
        reset_and_start_timer();
        executeRock(original, result.getBuffer());
        double t = get_elapsed_mcycles();
        printf("@time of ROCK run:\t\t\t[%.3f] million cycles\n", t);
        minTimeRock = std::min(minTimeRock, t);
    }

    if (!verifyImage(reference, result, 0.006)) {
        printf("ERROR: ROCK verification failed!\n");
    }

    saveImage("data\\result_rock.bmp", result);

    //
    // Run reference now without saving intermediate files, testIterations times, and report the minimum
    // time for any of them.
    //
    double minTimeIron = 1e30;
    for (unsigned int i = 0; i < testIterations; i++) {
        result.clear();
        reset_and_start_timer();
        executeIron(original, result.getBuffer());
        double t = get_elapsed_mcycles();
        printf("@time of IRON run:\t\t\t[%.3f] million cycles\n", t);
        minTimeIron = std::min(minTimeIron, t);
    }

    if (!verifyImage(reference, result, 0.006)) {
        printf("ERROR: IRON verification failed!\n");
    }

    saveImage("data\\result_iron.bmp", result);

    //
    // Run reference now without saving intermediate files, testIterations times, and report the minimum
    // time for any of them.
    //
    double minTimeDiamond = 1e30;
    for (unsigned int i = 0; i < testIterations; i++) {
        result.clear();
        reset_and_start_timer();
        executeDiamond(original, result.getBuffer());
        double t = get_elapsed_mcycles();
        printf("@time of DMND run:\t\t\t[%.3f] million cycles\n", t);
        minTimeDiamond = std::min(minTimeDiamond, t);
    }

    if (!verifyImage(reference, result, 0.006)) {
        printf("ERROR: DMND verification failed!\n");
    }

    saveImage("data\\result_diamond.bmp", result);

    //
    // Run reference now without saving intermediate files, testIterations times, and report the minimum
    // time for any of them.
    //
    double minTimePaper = 1e30;
    for (unsigned int i = 0; i < testIterations; i++) {
        result.clear();
        reset_and_start_timer();
        executePaper(original, result.getBuffer());
        double t = get_elapsed_mcycles();
        printf("@time of PAPR run:\t\t\t[%.3f] million cycles\n", t);
        minTimePaper = std::min(minTimePaper, t);
    }

    if (!verifyImage(reference, result, 0.006)) {
        printf("ERROR: PAPR verification failed!\n");
    }

    saveImage("data\\result_paper.bmp", result);

    printf("[WOOD]:\t\t[%.3f] million cycles\n", minTimeWood);
    printf("\t\t\t\tTimings %.2f %.2f %.2f %.2f %.2f\n", a12, a23, a34, a45, a56);
    printf("[ROCK]:\t\t[%.3f] million cycles\n", minTimeRock);
    printf("\t\t\t\t(%.2fx speedup from WOOD)\n", minTimeWood / minTimeRock);
    printf("\t\t\t\tTimings %.2f %.2f %.2f %.2f %.2f\n", b12, b23, b34, b45, b56);
    printf("[IRON]:\t\t[%.3f] million cycles\n", minTimeIron);
    printf("\t\t\t\t(%.2fx speedup from WOOD)\n", minTimeWood / minTimeIron);
    printf("\t\t\t\tTimings %.2f %.2f %.2f %.2f %.2f\n", c12, c23, c34, c45, c56);
    printf("[DMND]:\t\t[%.3f] million cycles\n", minTimeDiamond);
    printf("\t\t\t\t(%.2fx speedup from WOOD)\n", minTimeWood / minTimeDiamond);
    printf("\t\t\t\t(%.2fx speedup from IRON)\n", minTimeIron / minTimeDiamond);
    printf("\t\t\t\tTimings %.2f %.2f %.2f %.2f %.2f\n", d12, d23, d34, d45, d56);
    printf("[PAPR]:\t\t[%.3f] million cycles\n", minTimePaper);
    printf("\t\t\t\t(%.2fx speedup from WOOD)\n", minTimeWood / minTimePaper);
    printf("\t\t\t\t(%.2fx speedup from IRON)\n", minTimeIron / minTimePaper);
    printf("\t\t\t\tTimings %.2f %.2f %.2f %.2f %.2f\n", e12, e23, e34, e45, e56);

    getchar();
    return EXIT_SUCCESS;
}
