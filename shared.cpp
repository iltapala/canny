#include "shared.h"

extern double a12 = 10e30, a23 = 10e30, a34 = 10e30, a45 = 10e30, a56 = 10e30;
extern double b12 = 10e30, b23 = 10e30, b34 = 10e30, b45 = 10e30, b56 = 10e30;
extern double c12 = 10e30, c23 = 10e30, c34 = 10e30, c45 = 10e30, c56 = 10e30;
extern double d12 = 10e30, d23 = 10e30, d34 = 10e30, d45 = 10e30, d56 = 10e30;
extern double e12 = 10e30, e23 = 10e30, e34 = 10e30, e45 = 10e30, e56 = 10e30;

#pragma pack(push, 1)

struct BmpHeader
{
    unsigned short signature;
    unsigned int fileSize;
    unsigned int reserved;
    unsigned int pixelArrayOffset;
};

struct BitmapInfoHeader
{
    uint32_t headerSize;
    int32_t width;
    int32_t height;
    uint16_t colorPlanes;
    uint16_t bitsPerPixel;
    uint32_t compression;
    uint32_t sizeImage;
    int32_t  xPelsPerMeter;
    int32_t  yPelsPerMeter;
    uint32_t clrUsed;
    uint32_t clrImportant;
};

#pragma pack(pop)

Image loadTestImage(const char* name)
{
    FILE* file = fopen(name, "rb");
    if (!file)
    {
        printf("Failed to open bitmap '%s'\n", name);
        return{};
    }

    BmpHeader header;
    fread(&header, 1, sizeof(header), file);

    BitmapInfoHeader infoHeader;
    fread(&infoHeader, 1, sizeof(infoHeader), file);

    assert(infoHeader.width % 4 == 0);
    assert(infoHeader.bitsPerPixel % 8 == 0);
    assert(infoHeader.colorPlanes == 1);
    assert(infoHeader.compression == 0);
    assert(infoHeader.clrUsed == 0);

    Image image(infoHeader.width, infoHeader.height, infoHeader.bitsPerPixel);

    const auto numberOfBytes = infoHeader.width * infoHeader.height * (infoHeader.bitsPerPixel / 8);
    assert((infoHeader.sizeImage == numberOfBytes) || (infoHeader.sizeImage == 0));
    assert(image.getBufferSize() == numberOfBytes);

    fseek(file, header.pixelArrayOffset, SEEK_SET);
    fread(image.getRawData(), 1, numberOfBytes, file);
    fclose(file);

    return image;
}

void saveImage(const char* name, const Image& image)
{
    FILE* file = fopen(name, "wb");
    if (!file)
    {
        printf("Failed to open '%s' for writing", name);
        return;
    }

    const auto colorsUsed = image.getBitsPerPixel() <= 8 ? (1u << image.getBitsPerPixel()) : 0;

    BmpHeader header;
    header.signature = 'MB';
    header.reserved = 0;
    header.pixelArrayOffset = sizeof(BmpHeader) + sizeof(BitmapInfoHeader) + colorsUsed * sizeof(RGBQuad);
    header.fileSize = header.pixelArrayOffset + image.getBufferSize();
    fwrite(&header, 1, sizeof(header), file);

    BitmapInfoHeader infoHeader;
    infoHeader.bitsPerPixel = image.getBitsPerPixel();
    infoHeader.width = image.getWidth();
    infoHeader.height = image.getHeight();
    infoHeader.colorPlanes = 1;
    infoHeader.headerSize = sizeof(BitmapInfoHeader);
    infoHeader.compression = 0;
    infoHeader.sizeImage = image.getBufferSize();
    infoHeader.xPelsPerMeter = infoHeader.yPelsPerMeter = 3780;
    infoHeader.clrUsed = colorsUsed;
    infoHeader.clrImportant = 0;
    fwrite(&infoHeader, 1, sizeof(infoHeader), file);

    if (colorsUsed > 0) {
        std::vector<RGBQuad> colors(colorsUsed);
        const uint8_t step = 256u / (1u << image.getBitsPerPixel());
        for (size_t i = 0; i < colorsUsed; ++i) {
            const uint8_t v = static_cast<uint8_t>(i * step);
            colors[i] = RGBQuad{ v, v, v, 0 };
        }

        fwrite(colors.data(), 1, colorsUsed * sizeof(RGBQuad), file);
    }

    fwrite(image.getRawData(), 1, image.getBufferSize(), file);
    fclose(file);
}

bool verifyImage(const Image& reference, const Image& other, float allowedDifference)
{
    if ((reference.getWidth() != other.getWidth()) ||
        (reference.getHeight() != other.getHeight()) ||
        (reference.getBitsPerPixel() != other.getBitsPerPixel())) {
        return false;
    }

    const int MAX_PIXEL_DIFFERENCE = 8;

    assert(reference.getBitsPerPixel() == 8);
    double difference = 0.0;
    for (size_t y = 0; y < reference.getHeight(); ++y) {
        for (size_t x = 0; x < reference.getWidth(); ++x) {
            const auto v1 = reference.getBuffer().get(x, y);
            const auto v2 = other.getBuffer().get(x, y);
            difference += std::min(abs(static_cast<int>(v1) - static_cast<int>(v2)), MAX_PIXEL_DIFFERENCE);
        }
    }

    difference /= reference.getPixelCount() * MAX_PIXEL_DIFFERENCE;
    if (allowedDifference >= difference) {
        return true;
    }

    printf("Expected difference %.4f <= %.4f\n", difference, allowedDifference);
    return false;
}
