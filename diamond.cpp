#include "shared.h"
#include "timing.h"
#include "iron.h"
#include "rock.h"

#include "diamond_ispc.h"
using namespace ispc;

namespace diamond {


    void convertToGrayScale(const Image& input, ImageBuffer<uint8_t>& output)
    {
        switch (input.getBitsPerPixel())
        {
        case 24:
            diamond_grayscale_int_24(input.getWidth(), input.getHeight(), input.getRawData(), output.getRawData());
            break;
        case 32:
            diamond_grayscale_int_32(input.getWidth(), input.getHeight(), input.getRawData(), output.getRawData());
            break;
        default:
            return;
        }
    }

    void gaussBlur_4_s14(int w, int h, uint8_t* data) {
        std::vector<uint8_t> temp(w * h);
        uint8_t* tempData = &temp[0];

        diamond_boxblur_H1(w, h, data, tempData);
        diamond_boxblur_V1(w, h, tempData, data);
        diamond_boxblur_H1(w, h, data, tempData);
        diamond_boxblur_V1(w, h, tempData, data);
        diamond_boxblur_H1(w, h, data, tempData);
        diamond_boxblur_V1(w, h, tempData, data);
    }

    void calculateSobel(const uint8_t* input, int16_t* dx, int16_t* dy, size_t w, size_t h)
    {
        std::vector<int16_t> temp(w * h);
        diamond_sobelx_H(w, h, input, &temp[0]);
        diamond_sobelx_V(w, h, &temp[0], dx);
        diamond_sobely_H(w, h, input, &temp[0]);
        diamond_sobely_V(w, h, &temp[0], dy);
    }

    void calculateSobel(const ImageBuffer<uint8_t>& input,
        ImageBuffer<int16_t>& outputX, ImageBuffer<int16_t>& outputY)
    {
        calculateSobel(input.getRawData(), outputX.getRawData(), outputY.getRawData(), input.getWidth(), input.getHeight());
    }

    void calculateGradient(const ImageBuffer<uint8_t>& input, ImageBuffer<int16_t>& Gx, ImageBuffer<int16_t>& Gy,
        ImageBuffer<int>& G)
    {
        // Make sobel convolutions
        calculateSobel(input, Gx, Gy);

        diamond_magnitude(input.getWidth(), input.getHeight(), Gx.getRawData(), Gy.getRawData(), G.getRawData());
    }
}

using namespace diamond;

void executeDiamond(const Image& original, ImageBuffer<uint8_t>& result)
{
    double t1 = get_elapsed_mcycles();
    const int w = original.getWidth();
    const int h = original.getHeight();
    ImageBuffer<uint8_t> data(w, h);
    //iron::convertToGrayScale(original, data);
    convertToGrayScale(original, data);
    double t2 = get_elapsed_mcycles();
    d12 = std::min(d12, t2 - t1);

    //iron::gaussBlur_4_s14(data.getData(), w, h);
    diamond::gaussBlur_4_s14(w, h, data.getRawData());
    double t3 = get_elapsed_mcycles();
    d23 = std::min(d23, t3 - t2);

    ImageBuffer<int> G(w, h);
    ImageBuffer<int16_t> Gx(w, h);
    ImageBuffer<int16_t> Gy(w, h);
    calculateGradient(data, Gx, Gy, G);
    double t4 = get_elapsed_mcycles();
    d34 = std::min(d34, t4 - t3);

    std::stack<std::pair<int, int>> strong;
    int Gmax = diamond_max(w, h, G.getRawData());
    rock::nonMaxSuppressionAndThreshold2(
        G, Gx, Gy, result, Gmax, 0.3f, 0.275f, strong);
    double t5 = get_elapsed_mcycles();
    d45 = std::min(d45, t5 - t4);

    rock::hysteresis(result, strong);
    diamond_remove_weak(result.getWidth(), result.getHeight(), result.getRawData());
    double t6 = get_elapsed_mcycles();
    d56 = std::min(d56, t6 - t5);
}

// Integer gaussian
// Strong pushed in threshold
// Combined non-max supression and threshold steps
// Use gradient magnitude
// Custom SobelX implementation
// Bithack trick from openCV to avoid trigonometry in non-max suppression quadrant selection
// Combined custom SobelXY implementation
// Integer grayscale
// Separated gaussian kernel
// Unrolled convolution to avoid safeGet
// Removed safeGets from hysteresis
// Changed weak clearing to use one dimensional loop
// Changed gradient magnitude calculation to use one dimensional loop
// Approximate gaussian with box blur
