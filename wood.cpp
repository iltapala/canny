#include "shared.h"
#include "timing.h"

namespace {

uint8_t grayscale(const RGB& c)
{
    // https://rosettacode.org/wiki/Grayscale_image
    //return static_cast<uint8_t>(0.2126f * c.r + 0.7152f * c.g + 0.0722 * c.b);
    
    //https://www.mathworks.com/help/matlab/ref/rgb2gray.html
    return static_cast<uint8_t>(0.2989f * c.r + 0.5870f * c.g + 0.1140f * c.b);
}

Image convertToGrayScale(const Image& input)
{
    Image result(input.getWidth(), input.getHeight(), 8);
    auto& buffer = result.getBuffer();
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            buffer.set(x, y, grayscale(input.getRGB(x, y)));
        }
    }

    return result;
}

template <typename T, typename U>
U clamp(T value);

template <> uint8_t clamp(float value) {
    const auto v = static_cast<int>(roundf(value));
    if (v < 0) {
        return 0;
    }
    else if (v > 255) {
        return 255;
    }
    else {
        return static_cast<uint8_t>(v);
    }
}

template <> uint8_t clamp(int value) {
    if (value < 0) {
        return 0;
    }
    else if (value > 255) {
        return 255;
    }
    else {
        return static_cast<uint8_t>(value);
    }
}

template <> int clamp(int value) {
    return value;
}

template <> int clamp(float value) {
    return static_cast<int>(roundf(value));
}

template <typename T, int N>
class Kernel
{
public:
    static constexpr int MAX = N / 2;
    static constexpr int MIN = -MAX;

    inline T get(int x, int y) const
    {
        return m_data[x + y * N + OFFSET];
    }

    inline void set(int x, int y, T value)
    {
        m_data[x + y * N + OFFSET] = value;
    }

    inline T sum() const
    {
        T s = 0;
        for (size_t i = 0; i < N * N; ++i) {
            s += m_data[i];
        }

        return s;
    }

    inline Kernel& operator*=(T multiplier)
    {
        for (size_t i = 0; i < N * N; ++i) {
            m_data[i] *= multiplier;
        }
        return *this;
    }

    inline Kernel& operator/=(T divider)
    {
        *this *= (1.0 / divider);
        return *this;
    }

    Image apply(const ImageBuffer<uint8_t>& input) const
    {
        Image result(input.getWidth(), input.getHeight(), 8);
        convolute(input, result.getBuffer());

        return result;
    }

    template <typename A, typename B>
    void convolute(const ImageBuffer<A>& input, ImageBuffer<B>& output) const
    {
        assert((input.getWidth() == output.getWidth()) && (input.getHeight() == output.getHeight()));

        for (int y = 0; y < input.getHeight(); ++y) {
            for (int x = 0; x < input.getWidth(); ++x) {
                T sum = 0;
                for (int ky = MIN; ky <= MAX; ++ky) {
                    for (int kx = MIN; kx <= MAX; ++kx) {
                        sum += get(kx, ky) * input.safeGet(x + kx, y + ky);
                    }
                }

                output.set(x, y, clamp<T, B>(sum));
            }
        }
    }

private:
    static constexpr int OFFSET = N * N / 2;
    T m_data[N * N]{ 0 };
};

template <int N>
class Kernel<int, N>
{
public:
    static constexpr int MAX = N / 2;
    static constexpr int MIN = -MAX;

    Kernel(int denominator, const std::initializer_list<int>& m)
        : m_denominator(denominator)
    {
        assert(m.size() == N * N);
        for (size_t i = 0; i < N * N; ++i) {
            m_data[i] = *(m.begin() + i);
        }
    }

    inline int get(int x, int y) const
    {
        return m_data[x + y * N + OFFSET];
    }

    inline void set(int x, int y, int value)
    {
        m_data[x + y * N + OFFSET] = value;
    }

    inline int sum() const
    {
        int s = 0;
        for (size_t i = 0; i < N * N; ++i) {
            s += m_data[i];
        }

        return s;
    }

    Image apply(const ImageBuffer<uint8_t>& input) const
    {
        Image result(input.getWidth(), input.getHeight(), 8);
        convolute(input, result.getBuffer());

        return result;
    }

    template <typename A, typename B>
    void convolute(const ImageBuffer<A>& input, ImageBuffer<B>& output) const
    {
        assert((input.getWidth() == output.getWidth()) && (input.getHeight() == output.getHeight()));

        for (int y = 0; y < input.getHeight(); ++y) {
            for (int x = 0; x < input.getWidth(); ++x) {
                int sum = 0;
                for (int ky = MIN; ky <= MAX; ++ky) {
                    for (int kx = MIN; kx <= MAX; ++kx) {
                        sum += get(kx, ky) * input.safeGet(x + kx, y + ky);
                    }
                }

                output.set(x, y, clamp<int, B>((sum + (m_denominator / 2)) / m_denominator));
            }
        }
    }

private:
    static constexpr int OFFSET = N * N / 2;
    int m_denominator;
    int m_data[N * N]{ 0 };
};

template <size_t N>
Kernel<float, N> createGaussianKernel(float sigma)
{
    using DKernel = Kernel<double, N>;
    using Kernel = Kernel<float, N>;
    const auto denominator = 1.0f / (2.0f * sigma * sigma);
    //const auto normal = denominator / M_PI;

    Kernel kernel;
    for (int y = Kernel::MIN; y <= Kernel::MAX; ++y) {
        for (int x = Kernel::MIN; x <= Kernel::MAX; ++x) {
            kernel.set(x, y, exp((x * x + y * y) * -denominator));// *normal);
        }
    }

    const auto sum = kernel.sum();
    kernel /= sum;
    return kernel;
}

static constexpr std::initializer_list<int> gaussian5x5 = {
  2, 4, 5, 4, 2,
  4, 9, 12, 9, 4,
  5, 12, 15, 12, 5,
  4, 9, 12, 9, 4,
  2, 4, 5, 4, 2,
};

static const Kernel<int, 5> gaussianKernel5(159, gaussian5x5);

template <typename T>
void gaussianBlur5x5(const ImageBuffer<T>& input, ImageBuffer<T>& output)
{
    return createGaussianKernel<5>(1.4f).convolute(input, output);
    //return gaussianKernel5.convolute(input, output);
    //return createGaussianKernel<5>(1.0f).convolute(input, output);
}

template <typename T>
Image gaussianBlur5x5Image(const ImageBuffer<T>& input)
{
    Image result(input.getWidth(), input.getHeight(), 8);
    gaussianBlur5x5(input, result.getBuffer());
    return result;
}

template <typename T>
Image normalize(const ImageBuffer<T>& input)
{
    const auto maximum = input.max();

    Image result(input.getWidth(), input.getHeight(), 8);
    auto& buffer = result.getBuffer();
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto value = input.get(x, y);
            buffer.set(x, y, static_cast<uint8_t>(value * 255 / maximum));
        }
    }

    return result;
}

template <typename T>
Image normalizeSigned(const ImageBuffer<T>& input)
{
    T minimum = std::numeric_limits<T>::max();
    T maximum = std::numeric_limits<T>::min();
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto value = input.get(x, y);
            minimum = std::min(minimum, value);
            maximum = std::max(maximum, value);
        }
    }

    const T scale = std::max(abs(minimum), maximum);

    Image result(input.getWidth(), input.getHeight(), 8);
    auto& buffer = result.getBuffer();
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto value = input.get(x, y);
            buffer.set(x, y, static_cast<uint8_t>(round(127.5f + value * 127.5f / scale)));
        }
    }

    return result;
}

template <typename T>
ImageBuffer<T> absolute(const ImageBuffer<T>& input)
{
    ImageBuffer<T> result(input.getWidth(), input.getHeight());
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto value = input.get(x, y);
            result.set(x, y, abs(value));
        }
    }

    return result;
}

static constexpr std::initializer_list<int> sobelX = {
  1, 0, -1,
  2, 0, -2,
  1, 0, -1
};

static constexpr std::initializer_list<int> sobelY = {
  1, 2, 1,
  0, 0, 0,
  -1, -2, -1
};

static const Kernel<int, 3> sobelXKernel(1, sobelX);
static const Kernel<int, 3> sobelYKernel(1, sobelY);

void calculateGradient(const Image& input, ImageBuffer<int>& Gx, ImageBuffer<int>& Gy,
    ImageBuffer<float>& G, ImageBuffer<float>& theta)
{
    // Make sobel convolutions
    //ImageBuffer<int> Gx(input.getWidth(), input.getHeight());
    //ImageBuffer<int> Gy(input.getWidth(), input.getHeight());
    /*{
        ImageBuffer<int> temp(input.getWidth(), input.getHeight());
        sobelXKernel.convolute(input.getBuffer(), temp);
        gaussianBlur5x5(temp, Gx);
    }
    {
        ImageBuffer<int> temp(input.getWidth(), input.getHeight());
        sobelYKernel.convolute(input.getBuffer(), temp);
        gaussianBlur5x5(temp, Gy);
    }*/
    {
        sobelXKernel.convolute(input.getBuffer(), Gx);
        sobelYKernel.convolute(input.getBuffer(), Gy);
    }

    // Calculate hypotenusa and angle
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto ix = Gx.get(x, y);
            const auto iy = Gy.get(x, y);

            const float value = hypotf(ix, iy);
            G.set(x, y, value);
            theta.set(x, y, atan2f(iy, ix));
        }
    }
}

// This is non-interpolating version. There is also interpolating version
// https://github.com/JustinLiang/ComputerVisionProjects/blob/master/CannyEdgeDetector/CannyEdgeDetector.m
template <typename T>
Image nonMaxSuppression(const ImageBuffer<T>& input, const ImageBuffer<T>& theta)
{
    Image result(input.getWidth(), input.getHeight(), 8);
    auto& buffer = result.getBuffer();

    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            float angle = theta.get(x, y) * 180.0f / M_PI;
            if (angle < 0.0f) angle += 180.0f;

            T q, r;

            // angle 0
            if ((angle < 22.5f) || (angle >= 157.5f)) {
                q = input.safeGet(x + 1, y);
                r = input.safeGet(x - 1, y);
            }
            // angle 45
            else if (angle < 67.5f) {
                q = input.safeGet(x + 1, y + 1);
                r = input.safeGet(x - 1, y - 1);
            }
            // angle 90
            else if (angle < 112.5f) {
                q = input.safeGet(x, y + 1);
                r = input.safeGet(x, y - 1);
            }
            // angle 135
            else {
                q = input.safeGet(x - 1, y + 1);
                r = input.safeGet(x + 1, y - 1);
            }

            const auto v = input.get(x, y);
            buffer.set(x, y, ((v >= q) && (v >= r)) ? clamp<T, uint8_t>(v) : 0);
        }
    }

    return result;
}

// This is interpolating version.
// https://github.com/JustinLiang/ComputerVisionProjects/blob/master/CannyEdgeDetector/CannyEdgeDetector.m
template <typename T, typename U>
Image nonMaxSuppressionInterpolate(
    const ImageBuffer<U>& Gx, const ImageBuffer<U>& Gy,
    const ImageBuffer<T>& input, const ImageBuffer<T>& theta)
{
    Image result(input.getWidth(), input.getHeight(), 8);
    auto& buffer = result.getBuffer();

    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto v = input.get(x, y);
            if (abs(v) < 1e-4f) {
                buffer.set(x, y, 0);
                continue;
            }

            float angle = theta.get(x, y) * 180.0f / M_PI;
            //if (angle < 0.0f) angle += 180.0f;

            T q, r;
            if ((angle >= 0 && angle <= 45) || (angle < -135 && angle >= -180)) {
                const auto b1 = input.safeGet(x, y + 1);
                const auto b2 = input.safeGet(x + 1, y + 1);
                const auto t1 = input.safeGet(x, y - 1);
                const auto t2 = input.safeGet(x - 1, y - 1);
                const auto f = abs(Gy.get(x, y) / v);
                q = (b2 - b1) * f + b1;
                r = (t2 - t1) * f + t1;
            }
            else if ((angle > 45 && angle <= 90) || (angle < -90 && angle >= -135)) {
                const auto b1 = input.safeGet(x + 1, y);
                const auto b2 = input.safeGet(x + 1, y + 1);
                const auto t1 = input.safeGet(x - 1, y);
                const auto t2 = input.safeGet(x - 1, y - 1);
                const auto f = abs(Gx.get(x, y) / v);
                q = (b2 - b1) * f + b1;
                r = (t2 - t1) * f + t1;
            }
            else if ((angle > 90 && angle <= 135) || (angle < -45 && angle >= -90)) {
                const auto b1 = input.safeGet(x + 1, y);
                const auto b2 = input.safeGet(x + 1, y - 1);
                const auto t1 = input.safeGet(x - 1, y);
                const auto t2 = input.safeGet(x - 1, y + 1);
                const auto f = abs(Gx.get(x, y) / v);
                //const auto f = abs(Gy.get(x, y) / v);
                q = (b2 - b1) * f + b1;
                r = (t2 - t1) * f + t1;
            }
            else //if ((angle > 135 && angle <= 180) || (angle < 0 && angle >= -45))
            {
                const auto b1 = input.safeGet(x, y - 1);
                const auto b2 = input.safeGet(x + 1, y - 1);
                const auto t1 = input.safeGet(x, y + 1);
                const auto t2 = input.safeGet(x - 1, y + 1);
                const auto f = abs(Gx.get(x, y) / v);
                q = (b2 - b1) * f + b1;
                r = (t2 - t1) * f + t1;
            }

            buffer.set(x, y, ((v >= q) && (v >= r)) ? v : 0);
        }
    }

    return result;
}

const uint8_t STRONG = 255;
const uint8_t WEAK = 144;

Image threshold(const ImageBuffer<uint8_t>& input, float lowThresholdRatio, float highThresholdRatio)
{
    const uint8_t maximum = input.max();
    const uint8_t highThreshold = static_cast<uint8_t>(maximum * highThresholdRatio);
    const uint8_t lowThreshold = static_cast<uint8_t>(highThreshold * lowThresholdRatio);

    Image result(input.getWidth(), input.getHeight(), 8);
    auto& buffer = result.getBuffer();

    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto v = input.get(x, y);
            uint8_t c;
            if (v >= highThreshold) {
                c = STRONG;
            }
            else if (v >= lowThreshold) {
                c = WEAK;
            }
            else {
                c = 0;
            }

            buffer.set(x, y, c);
        }
    }

    return result;
}

void hysteresis(ImageBuffer<uint8_t>& input)
{
    // Find strong pixels
    std::stack<std::pair<size_t, size_t>> stack;
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto v = input.get(x, y);
            if (v == STRONG) {
                stack.push(std::make_pair(x, y));
            }
        }
    }

    // Color weak edges next to strong edges
    while (!stack.empty())
    {
        const auto p = stack.top();
        stack.pop();

        const auto x = p.first;
        const auto y = p.second;

        if (input.safeGetZ(x - 1, y - 1) == WEAK) { input.set(x - 1, y - 1, STRONG); stack.push(std::make_pair(x - 1, y - 1)); }
        if (input.safeGetZ(x, y - 1) == WEAK) { input.set(x, y - 1, STRONG); stack.push(std::make_pair(x, y - 1)); }
        if (input.safeGetZ(x + 1, y - 1) == WEAK) { input.set(x + 1, y - 1, STRONG); stack.push(std::make_pair(x + 1, y - 1)); }
        if (input.safeGetZ(x - 1, y) == WEAK) { input.set(x - 1, y, STRONG); stack.push(std::make_pair(x - 1, y)); }
        if (input.safeGetZ(x + 1, y) == WEAK) { input.set(x + 1, y, STRONG); stack.push(std::make_pair(x + 1, y)); }
        if (input.safeGetZ(x - 1, y + 1) == WEAK) { input.set(x - 1, y + 1, STRONG); stack.push(std::make_pair(x - 1, y + 1)); }
        if (input.safeGetZ(x, y + 1) == WEAK) { input.set(x, y + 1, STRONG); stack.push(std::make_pair(x, y + 1)); }
        if (input.safeGetZ(x + 1, y + 1) == WEAK) { input.set(x + 1, y + 1, STRONG); stack.push(std::make_pair(x + 1, y + 1)); }
    }

    // Remove rest of the weak edges
    for (size_t y = 0; y < input.getHeight(); ++y) {
        for (size_t x = 0; x < input.getWidth(); ++x) {
            const auto v = input.get(x, y);
            if (v == WEAK) {
                input.set(x, y, 0);
            }
        }
    }
}
}

void executeWood(const Image& original, ImageBuffer<uint8_t>& result, bool saveIntermediateSteps)
{
    double t1 = get_elapsed_mcycles();
    auto data = convertToGrayScale(original);
    a12 = std::min(a12, get_elapsed_mcycles() - t1);
    if (saveIntermediateSteps) saveImage("data\\step1_grayscale.bmp", data);

    double t2 = get_elapsed_mcycles();
    data = gaussianBlur5x5Image(data.getBuffer());
    a23 = std::min(a23, get_elapsed_mcycles() - t2);
    if (saveIntermediateSteps) saveImage("data\\step2_gaussianblur.bmp", data);

    double t3 = get_elapsed_mcycles();
    ImageBuffer<float> G(data.getWidth(), data.getHeight());
    ImageBuffer<int> Gx(data.getWidth(), data.getHeight());
    ImageBuffer<int> Gy(data.getWidth(), data.getHeight());
    ImageBuffer<float> theta(data.getWidth(), data.getHeight());
    calculateGradient(data, Gx, Gy, G, theta);
    a34 = std::min(a34, get_elapsed_mcycles() - t3);

    if (saveIntermediateSteps) saveImage("data\\step3_gradient_horizontal.bmp", normalizeSigned(Gx));
    if (saveIntermediateSteps) saveImage("data\\step4_gradient_vertical.bmp", normalizeSigned(Gy));
    if (saveIntermediateSteps) saveImage("data\\step5_gradient_combined.bmp", normalize(G));

    double t4 = get_elapsed_mcycles();
    const auto temp = nonMaxSuppression(G, theta);
    //const auto temp = nonMaxSuppressionInterpolate(Gx, Gy, G, theta);

    data = threshold(temp.getBuffer(), 0.3f, 0.275f);
    a45 = std::min(a45, get_elapsed_mcycles() - t4);
    if (saveIntermediateSteps) saveImage("data\\step6_non_max_suppression.bmp", temp);
    if (saveIntermediateSteps) saveImage("data\\step7_threshold.bmp", data);

    double t5 = get_elapsed_mcycles();
    hysteresis(data.getBuffer());
    a56 = std::min(a56, get_elapsed_mcycles() - t5);

    if (saveIntermediateSteps) saveImage("data\\step8_hysteresis.bmp", data);

    result = data.getBuffer();
}
