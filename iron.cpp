#include "shared.h"
#include "timing.h"
#include "rock.h"

namespace iron {
    
    void convertToGrayscale_24(int w, int h, const uint8_t* input, uint8_t* output)
    {
        for (int i = 0; i < w * h; ++i) {
            const BGR& c = *reinterpret_cast<const BGR*>(&input[0]);
            input += 3;
            output[i] = static_cast<uint8_t>((2989 * c.r + 5870 * c.g + 1140 * c.b) / 10000);
        }
    }

    void convertToGrayscale_32(int w, int h, const uint8_t* input, uint8_t* output)
    {
        for (int i = 0; i < w * h; ++i) {
            const BGR& c = *reinterpret_cast<const BGR*>(&input[0]);
            input += 4;
            output[i] = static_cast<uint8_t>((2989 * c.r + 5870 * c.g + 1140 * c.b) / 10000);
        }
    }
    /*
    void convertToGrayscale_24(int w, int h, const uint8_t* input, uint8_t* output)
    {
        for (int i = 0; i < w * h; ++i) {
            const BGR& c = *reinterpret_cast<const BGR*>(&input[0]);
            input += 3;
            const uint16_t v = 77u * c.r + 150u * c.g + 29u * c.b;
            output[i] = static_cast<uint8_t>(v >> 8);
        }
    }

    void convertToGrayscale_32(int w, int h, const uint8_t* input, uint8_t* output)
    {
        for (int i = 0; i < w * h; ++i) {
            const BGR& c = *reinterpret_cast<const BGR*>(&input[0]);
            input += 4;
            const uint16_t v = 77u * c.r + 150u * c.g + 29u * c.b;
            output[i] = static_cast<uint8_t>(v >> 8);
        }
    }
    */
    void convertToGrayScale(const Image& input, ImageBuffer<uint8_t>& output)
    {
        switch (input.getBitsPerPixel())
        {
        case 24:
            convertToGrayscale_24(input.getWidth(), input.getHeight(), input.getRawData(), output.getRawData());
            break;
        case 32:
            convertToGrayscale_32(input.getWidth(), input.getHeight(), input.getRawData(), output.getRawData());
            break;
        default:
            return;
        }
    }

    std::vector<size_t> boxesForGauss(float sigma, size_t n)  // standard deviation, number of boxes
    {
        auto wIdeal = sqrt((12 * sigma*sigma / n) + 1);  // Ideal averaging filter width 
        size_t wl = floor(wIdeal);
        if (wl % 2 == 0)
            wl--;
        size_t wu = wl + 2;

        auto mIdeal = (12 * sigma*sigma - n * wl*wl - 4 * n*wl - 3 * n) / (-4 * wl - 4);
        size_t m = round(mIdeal);
        // var sigmaActual = Math.sqrt( (m*wl*wl + (n-m)*wu*wu - n)/12 );

        std::vector<size_t> sizes(n);
        for (size_t i = 0; i < n; i++)
            sizes[i] = i < m ? wl : wu;
        return sizes;
    }

    void boxBlurH_4(std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h, size_t r) {
        const auto divider = r + r + 1;
        for (auto i = 0; i < h; i++) {
            auto ti = i * w, li = ti, ri = ti + r;
            const auto fv = scl[ti], lv = scl[ti + w - 1];
            auto val = (r + 1)*fv + divider / 2;
            for (auto j = 0; j < r; j++) val += scl[ti + j];
            for (auto j = 0; j <= r; j++) { val += scl[ri++] - fv;   tcl[ti++] = val / divider; }
            for (auto j = r + 1; j < w - r; j++) { val += scl[ri++] - scl[li++];   tcl[ti++] = val / divider; }
            for (auto j = w - r; j < w; j++) { val += lv - scl[li++];   tcl[ti++] = val / divider; }
        }
    }
    void boxBlurT_4(std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h, size_t r) {
        const auto divider = r + r + 1;
        for (size_t i = 0; i < w; i++) {
            auto ti = i, li = ti, ri = ti + r * w;
            const auto fv = scl[ti], lv = scl[ti + w * (h - 1)];
            auto val = (r + 1)*fv + divider / 2;
            for (auto j = 0; j < r; j++) val += scl[ti + j * w];
            for (auto j = 0; j <= r; j++) { val += scl[ri] - fv;  tcl[ti] = val / divider;  ri += w; ti += w; }
            for (auto j = r + 1; j < h - r; j++) { val += scl[ri] - scl[li];  tcl[ti] = val / divider;  li += w; ri += w; ti += w; }
            for (auto j = h - r; j < h; j++) { val += lv - scl[li];  tcl[ti] = val / divider;  li += w; ti += w; }
        }
    }
    void boxBlur_4(std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h, size_t r) {
        tcl = scl;
        boxBlurH_4(tcl, scl, w, h, r);
        boxBlurT_4(scl, tcl, w, h, r);
    }
    void gaussBlur_4(
        std::vector<unsigned char>& data, size_t w, size_t h, float r, size_t boxes = 3) {
        const auto bxs = boxesForGauss(r, boxes);
        std::vector<uint8_t> temp(w * h);
        size_t i = 0;
        if (boxes & 1) {
            boxBlur_4(data, temp, w, h, (bxs[0] - 1) / 2);
            data = temp;
            i = 1;
        }
        for (; i < boxes; i += 2) {
            boxBlur_4(data, temp, w, h, (bxs[i] - 1) / 2);
            boxBlur_4(temp, data, w, h, (bxs[i + 1] - 1) / 2);
        }
    }

    // Will modify input also
    void gaussianBlurBoxes(ImageBuffer<uint8_t>& inputOutput, float sigma, size_t boxes = 3)
    {
        gaussBlur_4(inputOutput.getData(), inputOutput.getWidth(), inputOutput.getHeight(), sigma, boxes);
    }

    void boxBlurH_4_r1(const std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h) {
        for (size_t i = 0; i < h; i++) {
            auto ti = i * w;
            const auto fv = scl[ti];
            const auto lv = scl[ti + w - 1];
            auto val = 3*fv + 1;
            int j = 0;
            for (; j <= 1; j++) { val += scl[ti + 1] - fv;   tcl[ti++] = val / 3; }
            for (; j < w - 1; j++) { val += scl[ti + 1] - scl[ti - 2];   tcl[ti++] = val / 3; }
            { val += lv - scl[ti - 2];   tcl[ti] = val / 3; }
        }
    }
    void boxBlurV_4_r1(const std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h) {
        for (size_t i = 0; i < w; i++) {
            auto ti = i;
            const auto fv = scl[i];
            const auto lv = scl[i + w * (h - 1)];
            auto val = 3*fv + 1;
            int j = 0;
            for (; j <= 1; j++) { val += scl[ti + w] - fv;  tcl[ti] = val / 3; ti += w; }
            for (; j < h - 1; j++) { val += scl[ti + w] - scl[ti - 2 * w];  tcl[ti] = val / 3; ti += w; }
            { val += lv - scl[ti - 2 * w]; tcl[ti] = val / 3; }
        }
    }

    void gaussBlur_4_s14(std::vector<unsigned char>& data, size_t w, size_t h) {
        std::vector<uint8_t> temp(w * h);
        boxBlurH_4_r1(data, temp, w, h);
        boxBlurV_4_r1(temp, data, w, h);
        boxBlurH_4_r1(data, temp, w, h);
        boxBlurV_4_r1(temp, data, w, h);
        boxBlurH_4_r1(data, temp, w, h);
        boxBlurV_4_r1(temp, data, w, h);
    }

    void gaussianBlurBoxes_s14(ImageBuffer<uint8_t>& inputOutput)
    {
        gaussBlur_4_s14(inputOutput.getData(), inputOutput.getWidth(), inputOutput.getHeight());
    }
}

using namespace iron;

void executeIron(const Image& original, ImageBuffer<uint8_t>& result)
{
    double t1 = get_elapsed_mcycles();
    ImageBuffer<uint8_t> data(original.getWidth(), original.getHeight());
    convertToGrayScale(original, data);
    double t2 = get_elapsed_mcycles();
    c12 = std::min(c12, t2 - t1);

    gaussianBlurBoxes_s14(data);
    //gaussianBlurBoxes(data, 1.4f);
    //gaussianBlurBoxes(data.getBuffer(), 1.4f, 3);
    double t3 = get_elapsed_mcycles();
    c23 = std::min(c23, t3 - t2);

    ImageBuffer<int> G(data.getWidth(), data.getHeight());
    ImageBuffer<int16_t> Gx(data.getWidth(), data.getHeight());
    ImageBuffer<int16_t> Gy(data.getWidth(), data.getHeight());
    rock::calculateGradient(data, Gx, Gy, G);
    double t4 = get_elapsed_mcycles();
    c34 = std::min(c34, t4 - t3);

    std::stack<std::pair<int, int>> strong;
    rock::nonMaxSuppressionAndThreshold2(
        G, Gx, Gy, result, G.max(), 0.3f, 0.275f, strong);
    double t5 = get_elapsed_mcycles();
    c45 = std::min(c45, t5 - t4);

    rock::hysteresis(result, strong);
    rock::removeWeak(result);
    double t6 = get_elapsed_mcycles();
    c56 = std::min(c56, t6 - t5);
}

// Integer gaussian
// Strong pushed in threshold
// Combined non-max supression and threshold steps
// Use gradient magnitude
// Custom SobelX implementation
// Bithack trick from openCV to avoid trigonometry in non-max suppression quadrant selection
// Combined custom SobelXY implementation
// Integer grayscale
// Separated gaussian kernel
// Unrolled convolution to avoid safeGet
// Removed safeGets from hysteresis
// Changed weak clearing to use one dimensional loop
// Changed gradient magnitude calculation to use one dimensional loop
// Approximate gaussian with box blur (http://blog.ivank.net/fastest-gaussian-blur.html)
// Grayscale conversion specialized for different bit depths
