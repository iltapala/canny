#include "shared.h"
#include "timing.h"
#include "rock.h"
#include "iron.h"

#include <intrin.h>
#include <emmintrin.h>

#include <cstdint>

namespace paper {
    void convertToGrayscale_24(int w, int h, const uint8_t* input, uint8_t* output)
    {
        // SSE2
        // Loop runs 16 pixels at a time (reads 6x64 bits, stores 128 bits)
        constexpr uint32_t VEC_LEN = 16;
        const int size = w * h;

        const __m128i scale1 = _mm_setr_epi16(19589, 38470, 7471, 19589, 38470, 7471, 19589, 38470);
        const __m128i scale2 = _mm_setr_epi16(7471, 19589, 38470, 7471, 19589, 38470, 7471, 19589);
        const __m128i scale3 = _mm_setr_epi16(38470, 7471, 19589, 38470, 7471, 19589, 38470, 7471);
        const __m128i zero = _mm_setzero_si128();

        const __m128i shuffle1 = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 0x80, 0x80, 6, 7, 8, 9, 10, 11, 0x80, 0x80);
        const __m128i shuffle2 = _mm_setr_epi8(0x80, 0x80, 0x80, 0x80, 0, 1, 0x80, 0x80, 2, 3, 4, 5, 6, 7, 0x80, 0x80);
        const __m128i shuffle3a = _mm_setr_epi8(8, 9, 10, 11, 12, 13, 0x80, 0x80, 14, 15, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80);
        const __m128i shuffle3b = _mm_setr_epi8(0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0, 1, 2, 3, 0x80, 0x80);
        const __m128i shuffle4 = _mm_setr_epi8(4, 5, 6, 7, 8, 9, 0x80, 0x80, 10, 11, 12, 13, 14, 15, 0x80, 0x80);

        int i = 0;
        for (; i + VEC_LEN <= size; i += VEC_LEN) {
            // Load 64-bits to lower half x 6
            __m128i in1 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input));      // r1 g1 b1 r2 g2 b2 r3 g3
            __m128i in2 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 8));  // b3 r4 g4 b4 r5 g5 b5 r6
            __m128i in3 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 16)); // g6 b6 r7 g7 b7 r8 g8 b8
            __m128i in4 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 24));
            __m128i in5 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 32));
            __m128i in6 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 40));
            input += 48;

            // Unpack them to 16-bit integers
            in1 = _mm_unpacklo_epi8(in1, zero);
            in2 = _mm_unpacklo_epi8(in2, zero);
            in3 = _mm_unpacklo_epi8(in3, zero);
            in4 = _mm_unpacklo_epi8(in4, zero);
            in5 = _mm_unpacklo_epi8(in5, zero);
            in6 = _mm_unpacklo_epi8(in6, zero);

            // Multiply by color scale (store high 16-bits of resulting 32-bit value)
            in1 = _mm_mulhi_epu16(in1, scale1);
            in2 = _mm_mulhi_epu16(in2, scale2);
            in3 = _mm_mulhi_epu16(in3, scale3);
            in4 = _mm_mulhi_epu16(in4, scale1);
            in5 = _mm_mulhi_epu16(in5, scale2);
            in6 = _mm_mulhi_epu16(in6, scale3);

            // Expand to separate pixels 64-bits each
            auto c1 = _mm_shuffle_epi8(in1, shuffle1);   // r1 g1 b1 0 r2 g2 b2 0
            auto c2a = _mm_bsrli_si128(in1, 12);         // r3 g3 0 0 0 0 0 0
            auto c2b = _mm_shuffle_epi8(in2, shuffle2);  // 0 0 b3 0 r4 g4 b4 0
            auto c2 = _mm_or_si128(c2a, c2b);            // r3 g3 b3 0 r4 g4 b4 0
            auto c3a = _mm_shuffle_epi8(in2, shuffle3a); // r5 g5 b5 0 r6 0 0 0
            auto c3b = _mm_shuffle_epi8(in3, shuffle3b); // 0 0 0 0 0 g6 b6 0
            auto c3 = _mm_or_si128(c3a, c3b);            // r5 g5 b5 0 r6 g6 b6 0
            auto c4 = _mm_shuffle_epi8(in3, shuffle4);   // r7 g7 b7 0 r8 g8 b8 0
            auto c5 = _mm_shuffle_epi8(in4, shuffle1);   // r1 g1 b1 0 r2 g2 b2 0
            auto c6a = _mm_bsrli_si128(in4, 12);         // r3 g3 0 0 0 0 0 0
            auto c6b = _mm_shuffle_epi8(in5, shuffle2);  // 0 0 b3 0 r4 g4 b4 0
            auto c6 = _mm_or_si128(c6a, c6b);            // r3 g3 b3 0 r4 g4 b4 0
            auto c7a = _mm_shuffle_epi8(in5, shuffle3a); // r5 g5 b5 0 r6 0 0 0
            auto c7b = _mm_shuffle_epi8(in6, shuffle3b); // 0 0 0 0 0 g6 b6 0
            auto c7 = _mm_or_si128(c7a, c7b);            // r5 g5 b5 0 r6 g6 b6 0
            auto c8 = _mm_shuffle_epi8(in6, shuffle4);   // r7 g7 b7 0 r8 g8 b8 0

            // Sum horizontally every 16-bit pair
            c1 = _mm_hadd_epi16(c1, c2);
            c3 = _mm_hadd_epi16(c3, c4);
            c5 = _mm_hadd_epi16(c5, c6);
            c7 = _mm_hadd_epi16(c7, c8);

            // Second horizontal sum of every 16-bit pair
            c1 = _mm_hadd_epi16(c1, c3);
            c5 = _mm_hadd_epi16(c5, c7);

            // Get low 8-bits of each grayscale value 
            c1 = _mm_packus_epi16(c1, c5);

            // Store full 128-bits
            _mm_store_si128(reinterpret_cast<__m128i*>(output), c1);
            output += 16;
        }

        // Do rest of the pixels
        for (; i < size; ++i) {
            const BGR& c = *reinterpret_cast<const BGR*>(&input[0]);
            input += 3;
            const uint16_t r = (19589u * c.r) >> 16;
            const uint16_t g = (38470u * c.g) >> 16;
            const uint16_t b = (7471u * c.b) >> 16;
            *output++ = static_cast<uint8_t>(r + g + b);
        }
    }

    void convertToGrayscale_32(int w, int h, const uint8_t* input, uint8_t* output)
    {
        // SSE2
        // Loop runs 16 pixels at a time (reads 8x64 bits, stores 128 bits)
        constexpr uint32_t VEC_LEN = 16;
        const int size = w * h;

        const __m128i scale = _mm_setr_epi16(19589, 38470, 7471, 0, 19589, 38470, 7471, 0);
        const __m128i zero = _mm_setzero_si128();

        int i = 0;
        for (; i + VEC_LEN < size; i += VEC_LEN) {
            // Load 64-bits to lower half x 8
            __m128i in1 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input));
            __m128i in2 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 8));
            __m128i in3 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 16));
            __m128i in4 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 24));
            __m128i in5 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 32));
            __m128i in6 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 40));
            __m128i in7 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 48));
            __m128i in8 = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input + 56));
            input += 64;

            // Unpack them to 16-bit integers
            in1 = _mm_unpacklo_epi8(in1, zero);
            in2 = _mm_unpacklo_epi8(in2, zero);
            in3 = _mm_unpacklo_epi8(in3, zero);
            in4 = _mm_unpacklo_epi8(in4, zero);
            in5 = _mm_unpacklo_epi8(in5, zero);
            in6 = _mm_unpacklo_epi8(in6, zero);
            in7 = _mm_unpacklo_epi8(in7, zero);
            in8 = _mm_unpacklo_epi8(in8, zero);

            // Multiply by color scale (store high 16-bits of resulting 32-bit value)
            in1 = _mm_mulhi_epu16(in1, scale);
            in2 = _mm_mulhi_epu16(in2, scale);
            in3 = _mm_mulhi_epu16(in3, scale);
            in4 = _mm_mulhi_epu16(in4, scale);
            in5 = _mm_mulhi_epu16(in5, scale);
            in6 = _mm_mulhi_epu16(in6, scale);
            in7 = _mm_mulhi_epu16(in7, scale);
            in8 = _mm_mulhi_epu16(in8, scale);

            // Sum horizontally every 16-bit pair
            in1 = _mm_hadd_epi16(in1, in2);
            in3 = _mm_hadd_epi16(in3, in4);
            in5 = _mm_hadd_epi16(in5, in6);
            in7 = _mm_hadd_epi16(in7, in8);

            // Second horizontal sum of every 16-bit pair
            in1 = _mm_hadd_epi16(in1, in3);
            in5 = _mm_hadd_epi16(in5, in7);

            // Get low 8-bits of each grayscale value 
            in1 = _mm_packus_epi16(in1, in5);

            // Store full 128-bits
            _mm_store_si128(reinterpret_cast<__m128i*>(output), in1);
            output += 16;
        }

        // Do rest of the pixels
        for (; i < size; ++i) {
            const BGR& c = *reinterpret_cast<const BGR*>(&input[0]);
            input += 4;
            const uint16_t r = (19589u * c.r) >> 16;
            const uint16_t g = (38470u * c.g) >> 16;
            const uint16_t b = (7471u * c.b) >> 16;
            output[i] = static_cast<uint8_t>(r + g + b);
        }
    }

    void convertToGrayScale(const Image& input, ImageBuffer<uint8_t>& output)
    {
        switch (input.getBitsPerPixel())
        {
        case 24: {
            /*
            const uint8_t test[] = {
                0xa1, 0xb1, 0xc1, 0xa2, 0xb2, 0xc2, 0xa3, 0xb3, 0xc3, 0xa4, 0xb4, 0xc4,
                0xa5, 0xb5, 0xc5, 0xa6, 0xb6, 0xc6, 0xa7, 0xb7, 0xc7, 0xa8, 0xb8, 0xc8,
                0xa9, 0xb9, 0xc9, 0xaa, 0xba, 0xca, 0xab, 0xbb, 0xcb, 0xac, 0xbc, 0xcc,
                0xad, 0xbd, 0xcd, 0xae, 0xbe, 0xce, 0xaf, 0xbf, 0xcf, 0xa0, 0xb0, 0xc0,
            };
            convertToGrayscale_24(16, 1, test, output.getRawData());
            */
            convertToGrayscale_24(input.getWidth(), input.getHeight(), input.getRawData(), output.getRawData());
            break;
        }
        case 32:
            convertToGrayscale_32(input.getWidth(), input.getHeight(), input.getRawData(), output.getRawData());
            break;
        default:
            return;
        }
    }

    void boxBlurH(const uint8_t* input, uint8_t* output, size_t w, size_t h)
    {
        constexpr size_t VEC_LEN = 16;

        const __m128i zero = _mm_setzero_si128();
        const __m128i oneThird = _mm_setr_epi16(0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556);
        const __m128i ones = _mm_setr_epi16(0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1);
        const __m128i first = _mm_setr_epi16(0xffff, 0, 0, 0, 0, 0, 0, 0);
        const __m128i last = _mm_setr_epi16(0, 0, 0, 0, 0, 0, 0, 0xffff);

        for (size_t y = 0; y < h; ++y) {
            int i = 0;

            if (VEC_LEN <= w) {
                // Load 64-bits of input
                __m128i prein = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // p0 ... p7
                input += VEC_LEN / 2;

                // Unpack as 16-bit values
                prein = _mm_unpacklo_epi8(prein, zero);
                __m128i prev0 = _mm_and_si128(prein, first);

                for (; i + 3*VEC_LEN/2 <= w; i += VEC_LEN) {
                    // Load 128-bits of input
                    __m128i in = _mm_load_si128(reinterpret_cast<const __m128i*>(input)); // d0 ... d15
                    input += VEC_LEN;

                    // Convert to 16-bit values
                    __m128i lo_in = _mm_unpacklo_epi8(in, zero);
                    __m128i hi_in = _mm_unpackhi_epi8(in, zero);

                    // Shifts
                    __m128i lo_cur = prein;
                    __m128i lo_prev = _mm_bslli_si128(lo_cur, 2);
                    __m128i lo_next = _mm_bsrli_si128(lo_cur, 2);
                    __m128i lo_next7 = _mm_bslli_si128(lo_in, 14);
                    __m128i hi_cur = lo_in;
                    __m128i hi_prev = _mm_bslli_si128(hi_cur, 2);
                    __m128i hi_next = _mm_bsrli_si128(hi_cur, 2);
                    __m128i hi_next7 = _mm_bslli_si128(hi_in, 14);

                    // SUM
                    // prev0 = p0  0  0  0  0  0  0  0 | p7  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6 |  0 d0
                    // +
                    // cur =   p0 p1 p2 p3 p4 p5 p6 p7 | d0 d1
                    // +
                    // next =  p1 p2 p3 p4 p5 p6 p7  0 | d1 d2
                    // +
                    // next7 =  0  0  0  0  0  0  0 d0 | 0  0
                    lo_prev = _mm_add_epi16(prev0, lo_prev);
                    lo_next = _mm_add_epi16(lo_next, lo_next7);
                    __m128i lo_sum = _mm_add_epi16(lo_prev, lo_cur);
                    lo_sum = _mm_add_epi16(lo_sum, lo_next);
                    __m128i hi_prev0 = _mm_bsrli_si128(lo_cur, 14);
                    hi_prev = _mm_add_epi16(hi_prev0, hi_prev);
                    hi_next = _mm_add_epi16(hi_next, hi_next7);
                    __m128i hi_sum = _mm_add_epi16(hi_prev, hi_cur);
                    hi_sum = _mm_add_epi16(hi_sum, hi_next);

                    // Advance
                    prev0 = _mm_bsrli_si128(hi_cur, 14);
                    prein = hi_in;

                    // Divide by three (i.e. multiply by 0x5556 and take high 16-bits)
                    __m128i lo_res = _mm_mulhi_epu16(_mm_add_epi16(lo_sum, ones), oneThird);
                    __m128i hi_res = _mm_mulhi_epu16(_mm_add_epi16(hi_sum, ones), oneThird);

                    // Pack
                    __m128i res = _mm_packus_epi16(lo_res, hi_res);

                    // Store 128-bits
                    _mm_store_si128(reinterpret_cast<__m128i*>(output), res);
                    output += VEC_LEN;
                }

                for (; i + VEC_LEN / 2 <= w; i += VEC_LEN / 2) {
                    __m128i in;
                    __m128i next7;

                    // Shifts
                    __m128i cur = prein;
                    __m128i prev = _mm_bslli_si128(cur, 2);
                    __m128i next = _mm_bsrli_si128(cur, 2);
                    if (i + VEC_LEN / 2 == w) {
                        in = zero;
                        next7 = _mm_and_si128(cur, last);
                    } else {
                        // Load 64-bits of input
                        in = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // d0 ... d7
                        input += VEC_LEN / 2;

                        // Convert to 16-bit values
                        in = _mm_unpacklo_epi8(in, zero);
                        next7 = _mm_bslli_si128(in, 14);
                    }

                    // SUM
                    // prev0 = p0  0  0  0  0  0  0  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6
                    // +
                    // cur =   p0 p1 p2 p3 p4 p5 p6 p7
                    // +
                    // next =  p1 p2 p3 p4 p5 p6 p7  0
                    // +
                    // next7 =  0  0  0  0  0  0  0 p7/d0
                    prev = _mm_add_epi16(prev0, prev);
                    next = _mm_add_epi16(next, next7);
                    __m128i sum = _mm_add_epi16(prev, cur);
                    sum = _mm_add_epi16(sum, next);

                    // Advance
                    prev0 = _mm_bsrli_si128(cur, 14);
                    prein = in;

                    // Divide by three (i.e. multiply by 0x5556 and take high 16-bits)
                    __m128i res = _mm_mulhi_epu16(_mm_add_epi16(sum, ones), oneThird);

                    // Pack
                    res = _mm_packus_epi16(res, zero);

                    // Store 64-bits
                    _mm_storel_epi64(reinterpret_cast<__m128i*>(output), res);
                    output += VEC_LEN / 2;
                }
            }

            if (i < w) {
                assert(false); // Not fully implemented or tested

                // Do the rest old fashion way
                uint16_t val;
                if (i == 0) {
                    const auto fv = *input;
                    val = 3 * fv + 1;
                    for (; i <= 1; i++) { val += input[+1] - fv; *output = val / 3; }
                }
                else {
                    val = 0;
                    //val = _mm_extract_epi16(in_lead3, 0) + _mm_extract_epi16(in_lead3, 1);
                }

                for (; i < w - 1; ++i) {
                    val = val + input[+1] - input[-2];
                    ++input;
                    *output++ = val / 3;
                }

                val = val + input[0] - input[-2];
                *output++ = val / 3;
            }
        }
    }

    void boxBlurH_64(const uint8_t* input, uint8_t* output, size_t w, size_t h)
    {
        // Unrolled x2, because there are at least two arithmetic units
        constexpr size_t VEC_LEN = 16 / 2;

        const __m128i zero = _mm_setzero_si128();
        const __m128i oneThird = _mm_setr_epi16(0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556);
        const __m128i ones = _mm_setr_epi16(0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1);
        const __m128i first = _mm_setr_epi16(0xffff, 0, 0, 0, 0, 0, 0, 0);
        const __m128i last = _mm_setr_epi16(0, 0, 0, 0, 0, 0, 0, 0xffff);

        for (size_t y = 0; y < h; ++y) {
            int i = 0;

            if (VEC_LEN <= w) {
                // Load 64-bits of input
                __m128i prein = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // p0 ... p7
                input += VEC_LEN;

                // Unpack as 16-bit values
                prein = _mm_unpacklo_epi8(prein, zero);
                __m128i prev0 = _mm_and_si128(prein, first);

                for (; i + 2 * VEC_LEN <= w; i += VEC_LEN) {
                    // Load 64-bits of input
                    __m128i in = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // d0 ... d7
                    input += VEC_LEN;

                    // Convert to 16-bit values
                    in = _mm_unpacklo_epi8(in, zero);

                    // Shifts
                    __m128i cur = prein;
                    __m128i prev = _mm_bslli_si128(cur, 2);
                    __m128i next = _mm_bsrli_si128(cur, 2);
                    __m128i next7 = _mm_bslli_si128(in, 14);

                    // SUM
                    // prev0 = p0  0  0  0  0  0  0  0 | p7  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6 |  0 d0
                    // +
                    // cur =   p0 p1 p2 p3 p4 p5 p6 p7 | d0 d1
                    // +
                    // next =  p1 p2 p3 p4 p5 p6 p7  0 | d1 d2
                    // +
                    // next7 =  0  0  0  0  0  0  0 d0 | 0  0
                    prev = _mm_add_epi16(prev0, prev);
                    next = _mm_add_epi16(next, next7);
                    __m128i sum = _mm_add_epi16(prev, cur);
                    sum = _mm_add_epi16(sum, next);

                    // Advance
                    prev0 = _mm_bsrli_si128(cur, 14);
                    prein = in;

                    // Divide by three (i.e. multiply by 0x5556 and take high 16-bits)
                    __m128i res = _mm_mulhi_epu16(_mm_add_epi16(sum, ones), oneThird);

                    // Pack
                    res = _mm_packus_epi16(res, zero);

                    // Store 64-bits
                    _mm_storel_epi64(reinterpret_cast<__m128i*>(output), res);
                    output += VEC_LEN;
                }

                {
                    // Shifts
                    __m128i cur = prein;
                    __m128i prev = _mm_bslli_si128(cur, 2);
                    __m128i next = _mm_bsrli_si128(cur, 2);
                    __m128i next7 = (i + VEC_LEN == w) ? _mm_and_si128(cur, last) :
                        _mm_setr_epi16(input[0], 0, 0, 0, 0, 0, 0, 0);

                    // SUM
                    // prev0 = p0  0  0  0  0  0  0  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6
                    // +
                    // cur =   p0 p1 p2 p3 p4 p5 p6 p7
                    // +
                    // next =  p1 p2 p3 p4 p5 p6 p7  0
                    // +
                    // next7 =  0  0  0  0  0  0  0 p7/d0
                    prev = _mm_add_epi16(prev0, prev);
                    next = _mm_add_epi16(next, next7);
                    __m128i sum = _mm_add_epi16(prev, cur);
                    sum = _mm_add_epi16(sum, next);

                    // Divide by three (i.e. multiply by 0x5556 and take high 16-bits)
                    __m128i res = _mm_mulhi_epu16(_mm_add_epi16(sum, ones), oneThird);

                    // Pack
                    res = _mm_packus_epi16(res, zero);

                    // Store 64-bits
                    _mm_storel_epi64(reinterpret_cast<__m128i*>(output), res);
                    output += VEC_LEN;
                    i += VEC_LEN;
                }
            }

            if (i < w) {
                assert(false); // Not fully implemented or tested

                // Do the rest old fashion way
                uint16_t val;
                if (i == 0) {
                    const auto fv = *input;
                    val = 3 * fv + 1;
                    for (; i <= 1; i++) { val += input[+1] - fv; *output = val / 3; }
                }
                else {
                    val = 0;
                    //val = _mm_extract_epi16(in_lead3, 0) + _mm_extract_epi16(in_lead3, 1);
                }

                for (; i < w - 1; ++i) {
                    val = val + input[+1] - input[-2];
                    ++input;
                    *output++ = val / 3;
                }

                val = val + input[0] - input[-2];
                *output++ = val / 3;
            }
        }
    }

    inline void boxBlurVLine(
        const uint8_t* prevLine, const uint8_t* currentLine, const uint8_t* nextLine, uint8_t* output, int w)
    {
        constexpr size_t VEC_LEN = 16;

        const __m128i zero = _mm_setzero_si128();
        const __m128i oneThird = _mm_setr_epi16(0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556, 0x5556);
        const __m128i ones = _mm_setr_epi16(0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1);

        int i = 0;
        for (; i + VEC_LEN <= w; i += VEC_LEN) {
            // Load 128-bits of input
            const __m128i prev = _mm_load_si128(reinterpret_cast<const __m128i*>(prevLine));
            prevLine += VEC_LEN;
            const __m128i current = _mm_load_si128(reinterpret_cast<const __m128i*>(currentLine));
            currentLine += VEC_LEN;
            const __m128i next = _mm_load_si128(reinterpret_cast<const __m128i*>(nextLine));
            nextLine += VEC_LEN;

            // Convert to 16-bit values
            const __m128i lo_prev = _mm_unpacklo_epi8(prev, zero);
            const __m128i hi_prev = _mm_unpackhi_epi8(prev, zero);
            const __m128i lo_cur = _mm_unpacklo_epi8(current, zero);
            const __m128i hi_cur = _mm_unpackhi_epi8(current, zero);
            const __m128i lo_next = _mm_unpacklo_epi8(next, zero);
            const __m128i hi_next = _mm_unpackhi_epi8(next, zero);

            // Sum
            __m128i lo_sum = _mm_add_epi16(lo_prev, lo_cur);
            __m128i hi_sum = _mm_add_epi16(hi_prev, hi_cur);
            lo_sum = _mm_add_epi16(lo_sum, lo_next);
            hi_sum = _mm_add_epi16(hi_sum, hi_next);

            // Divide by three (i.e. multiply by 0x5556 and take high 16-bits)
            __m128i lo_res = _mm_mulhi_epu16(_mm_add_epi16(lo_sum, ones), oneThird);
            __m128i hi_res = _mm_mulhi_epu16(_mm_add_epi16(hi_sum, ones), oneThird);

            // Pack
            __m128i res = _mm_packus_epi16(lo_res, hi_res);

            // Store 128-bits
            _mm_store_si128(reinterpret_cast<__m128i*>(output), res);
            output += VEC_LEN;
        }

        for (; i < w; i++) {
            const uint32_t sum = (uint16_t)*prevLine++ + (uint16_t)*currentLine++ + (uint16_t)*nextLine++ + 1;
            *output++ = (uint8_t)((sum * 0x5556u) >> 16);
        }
    }

    void boxBlurV(const uint8_t* input, uint8_t* output, size_t w, size_t h)
    {
        const uint8_t* prevLine = input;
        const uint8_t* currentLine = input;

        for (size_t y = 0; y < h - 1; ++y) {
            const uint8_t* nextLine = currentLine + w;
            boxBlurVLine(prevLine, currentLine, nextLine, output, w);
            output += w;
            prevLine = currentLine;
            currentLine = nextLine;
        }

        boxBlurVLine(prevLine, currentLine, currentLine, output, w);
    }

    void gaussianBlur(std::vector<unsigned char>& data, size_t w, size_t h) {
        std::vector<uint8_t> temp(w * h);
        boxBlurH(&data[0], &temp[0], w, h);
#ifndef NDEBUG
        std::vector<uint8_t> temp2(w * h);
        iron::boxBlurH_4_r1(data, temp2, w, h);
        for (size_t y = 0; y < h; ++y) {
            for (size_t x = 0; x < w; ++x) {
                if (temp[x + y * h] != temp2[x + y * h]) __debugbreak();
            }
        }
#endif
        boxBlurV(&temp[0], &data[0], w, h);
#ifndef NDEBUG
        iron::boxBlurV_4_r1(temp, temp2, w, h);
        for (size_t y = 0; y < h; ++y) {
            for (size_t x = 0; x < w; ++x) {
                if (data[x + y * h] != temp2[x + y * h]) __debugbreak();
            }
        }
#endif
        boxBlurH(&data[0], &temp[0], w, h);
        boxBlurV(&temp[0], &data[0], w, h);
        boxBlurH(&data[0], &temp[0], w, h);
        boxBlurV(&temp[0], &data[0], w, h);
    }

    void sobelXH(const uint8_t* input, int16_t* output, size_t w, size_t h)
    {
        constexpr size_t VEC_LEN = 16;

        const __m128i zero = _mm_setzero_si128();
        const __m128i first = _mm_setr_epi16(0xffff, 0, 0, 0, 0, 0, 0, 0);
        const __m128i last = _mm_setr_epi16(0, 0, 0, 0, 0, 0, 0, 0xffff);

        for (size_t y = 0; y < h; ++y) {
            int i = 0;

            if (VEC_LEN <= w) {
                // Load 64-bits of input
                __m128i prein = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // p0 ... p7
                input += VEC_LEN / 2;

                // Unpack as 16-bit values
                prein = _mm_unpacklo_epi8(prein, zero);
                __m128i prev0 = _mm_and_si128(prein, first);

                for (; i + 3 * VEC_LEN / 2 <= w; i += VEC_LEN) {
                    // Load 128-bits of input
                    __m128i in = _mm_load_si128(reinterpret_cast<const __m128i*>(input)); // d0 ... d15
                    input += VEC_LEN;

                    // Convert to 16-bit values
                    __m128i lo_in = _mm_unpacklo_epi8(in, zero);
                    __m128i hi_in = _mm_unpackhi_epi8(in, zero);

                    // Shifts
                    __m128i lo_cur = prein;
                    __m128i lo_prev = _mm_bslli_si128(lo_cur, 2);
                    __m128i lo_next = _mm_bsrli_si128(lo_cur, 2);
                    __m128i lo_next7 = _mm_bslli_si128(lo_in, 14);
                    __m128i hi_cur = lo_in;
                    __m128i hi_prev = _mm_bslli_si128(hi_cur, 2);
                    __m128i hi_next = _mm_bsrli_si128(hi_cur, 2);
                    __m128i hi_next7 = _mm_bslli_si128(hi_in, 14);

                    // DIFF
                    // (
                    // prev0 = p0  0  0  0  0  0  0  0 | p7  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6 |  0 d0
                    // )
                    // -
                    // (
                    // next =  p1 p2 p3 p4 p5 p6 p7  0 | d1 d2
                    // +
                    // next7 =  0  0  0  0  0  0  0 d0 | 0  0
                    // )
                    lo_prev = _mm_add_epi16(prev0, lo_prev);
                    lo_next = _mm_add_epi16(lo_next, lo_next7);
                    __m128i lo_diff = _mm_sub_epi16(lo_prev, lo_next);
                    __m128i hi_prev0 = _mm_bsrli_si128(lo_cur, 14);
                    hi_prev = _mm_add_epi16(hi_prev0, hi_prev);
                    hi_next = _mm_add_epi16(hi_next, hi_next7);
                    __m128i hi_diff = _mm_sub_epi16(hi_prev, hi_next);

                    // Advance
                    prev0 = _mm_bsrli_si128(hi_cur, 14);
                    prein = hi_in;

                    // Store 2x128-bits
                    _mm_store_si128(reinterpret_cast<__m128i*>(output), lo_diff);
                    _mm_store_si128(reinterpret_cast<__m128i*>(output + VEC_LEN / 2), hi_diff);
                    output += VEC_LEN;
                }

                for (; i + VEC_LEN / 2 <= w; i += VEC_LEN / 2) {
                    __m128i in;
                    __m128i next7;

                    // Shifts
                    __m128i cur = prein;
                    __m128i prev = _mm_bslli_si128(cur, 2);
                    __m128i next = _mm_bsrli_si128(cur, 2);
                    if (i + VEC_LEN / 2 == w) {
                        in = zero;
                        next7 = _mm_and_si128(cur, last);
                    }
                    else {
                        // Load 64-bits of input
                        in = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // d0 ... d7
                        input += VEC_LEN / 2;

                        // Convert to 16-bit values
                        in = _mm_unpacklo_epi8(in, zero);
                        next7 = _mm_bslli_si128(in, 14);
                    }

                    // DIFF
                    // (
                    // prev0 = p0  0  0  0  0  0  0  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6
                    // )
                    // -
                    // (
                    // next =  p1 p2 p3 p4 p5 p6 p7  0
                    // +
                    // next7 =  0  0  0  0  0  0  0 p7/d0
                    // )
                    prev = _mm_add_epi16(prev0, prev);
                    next = _mm_add_epi16(next, next7);
                    __m128i diff = _mm_sub_epi16(prev, next);

                    // Advance
                    prev0 = _mm_bsrli_si128(cur, 14);
                    prein = in;

                    // Store 128-bits
                    _mm_store_si128(reinterpret_cast<__m128i*>(output), diff);
                    output += VEC_LEN / 2;
                }
            }

            if (i < w) {
                assert(false); // Not implemented
            }
        }
    }

    inline void sobelXVLine(
        const int16_t* prevLine, const int16_t* currentLine, const int16_t* nextLine, int16_t* output, int w)
    {
        constexpr size_t VEC_LEN = 8;

        const __m128i zero = _mm_setzero_si128();

        int i = 0;
        for (; i + VEC_LEN <= w; i += VEC_LEN) {
            // Load 128-bits of input
            const __m128i prev = _mm_load_si128(reinterpret_cast<const __m128i*>(prevLine));
            prevLine += VEC_LEN;
            const __m128i current = _mm_load_si128(reinterpret_cast<const __m128i*>(currentLine));
            currentLine += VEC_LEN;
            const __m128i next = _mm_load_si128(reinterpret_cast<const __m128i*>(nextLine));
            nextLine += VEC_LEN;

            // prev + 2 x cur + next
            __m128i sum = _mm_add_epi16(prev, next);
            __m128i cur2 = _mm_add_epi16(current, current);
            sum = _mm_add_epi16(sum, cur2);

            // Store 128-bits
            _mm_store_si128(reinterpret_cast<__m128i*>(output), sum);
            output += VEC_LEN;
        }

        for (; i < w; i++) {
            const int16_t sum = *prevLine++ + 2 * *currentLine++ + *nextLine++;
            *output++ = sum;
        }
    }

    void sobelXV(const int16_t* input, int16_t* output, size_t w, size_t h)
    {
        const int16_t* prevLine = input;
        const int16_t* currentLine = input;

        for (size_t y = 0; y < h - 1; ++y) {
            const int16_t* nextLine = currentLine + w;
            sobelXVLine(prevLine, currentLine, nextLine, output, w);
            output += w;
            prevLine = currentLine;
            currentLine = nextLine;
        }

        sobelXVLine(prevLine, currentLine, currentLine, output, w);
    }

    void sobelYH(const uint8_t* input, int16_t* output, size_t w, size_t h)
    {
        constexpr size_t VEC_LEN = 16;

        const __m128i zero = _mm_setzero_si128();
        const __m128i first = _mm_setr_epi16(0xffff, 0, 0, 0, 0, 0, 0, 0);
        const __m128i last = _mm_setr_epi16(0, 0, 0, 0, 0, 0, 0, 0xffff);

        for (size_t y = 0; y < h; ++y) {
            int i = 0;

            if (VEC_LEN <= w) {
                // Load 64-bits of input
                __m128i prein = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // p0 ... p7
                input += VEC_LEN / 2;

                // Unpack as 16-bit values
                prein = _mm_unpacklo_epi8(prein, zero);
                __m128i prev0 = _mm_and_si128(prein, first);

                for (; i + 3 * VEC_LEN / 2 <= w; i += VEC_LEN) {
                    // Load 128-bits of input
                    __m128i in = _mm_load_si128(reinterpret_cast<const __m128i*>(input)); // d0 ... d15
                    input += VEC_LEN;

                    // Convert to 16-bit values
                    __m128i lo_in = _mm_unpacklo_epi8(in, zero);
                    __m128i hi_in = _mm_unpackhi_epi8(in, zero);

                    // Shifts
                    __m128i lo_cur = prein;
                    __m128i lo_prev = _mm_bslli_si128(lo_cur, 2);
                    __m128i lo_next = _mm_bsrli_si128(lo_cur, 2);
                    __m128i lo_next7 = _mm_bslli_si128(lo_in, 14);
                    __m128i hi_cur = lo_in;
                    __m128i hi_prev = _mm_bslli_si128(hi_cur, 2);
                    __m128i hi_next = _mm_bsrli_si128(hi_cur, 2);
                    __m128i hi_next7 = _mm_bslli_si128(hi_in, 14);

                    // SUM
                    // prev0 = p0  0  0  0  0  0  0  0 | p7  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6 |  0 d0
                    // +
                    // 2xcur =   p0 p1 p2 p3 p4 p5 p6 p7 | d0 d1
                    // +
                    // next =  p1 p2 p3 p4 p5 p6 p7  0 | d1 d2
                    // +
                    // next7 =  0  0  0  0  0  0  0 d0 | 0  0
                    lo_prev = _mm_add_epi16(prev0, lo_prev);
                    lo_next = _mm_add_epi16(lo_next, lo_next7);
                    __m128i lo_cur2 = _mm_add_epi16(lo_cur, lo_cur);
                    __m128i lo_sum = _mm_add_epi16(lo_prev, lo_next);
                    lo_sum = _mm_add_epi16(lo_sum, lo_cur2);
                    __m128i hi_prev0 = _mm_bsrli_si128(lo_cur, 14);
                    hi_prev = _mm_add_epi16(hi_prev0, hi_prev);
                    hi_next = _mm_add_epi16(hi_next, hi_next7);
                    __m128i hi_cur2 = _mm_add_epi16(hi_cur, hi_cur);
                    __m128i hi_sum = _mm_add_epi16(hi_prev, hi_next);
                    hi_sum = _mm_add_epi16(hi_sum, hi_cur2);

                    // Advance
                    prev0 = _mm_bsrli_si128(hi_cur, 14);
                    prein = hi_in;

                    // Store 2x128-bits
                    _mm_store_si128(reinterpret_cast<__m128i*>(output), lo_sum);
                    _mm_store_si128(reinterpret_cast<__m128i*>(output + VEC_LEN / 2), hi_sum);
                    output += VEC_LEN;
                }

                for (; i + VEC_LEN / 2 <= w; i += VEC_LEN / 2) {
                    __m128i in;
                    __m128i next7;

                    // Shifts
                    __m128i cur = prein;
                    __m128i prev = _mm_bslli_si128(cur, 2);
                    __m128i next = _mm_bsrli_si128(cur, 2);
                    if (i + VEC_LEN / 2 == w) {
                        in = zero;
                        next7 = _mm_and_si128(cur, last);
                    }
                    else {
                        // Load 64-bits of input
                        in = _mm_loadl_epi64(reinterpret_cast<const __m128i*>(input)); // d0 ... d7
                        input += VEC_LEN / 2;

                        // Convert to 16-bit values
                        in = _mm_unpacklo_epi8(in, zero);
                        next7 = _mm_bslli_si128(in, 14);
                    }

                    // SUM
                    // prev0 = p0  0  0  0  0  0  0  0 | p7  0
                    // +
                    // prev =   0 p0 p1 p2 p3 p4 p5 p6 |  0 d0
                    // +
                    // 2xcur =   p0 p1 p2 p3 p4 p5 p6 p7 | d0 d1
                    // +
                    // next =  p1 p2 p3 p4 p5 p6 p7  0 | d1 d2
                    // +
                    // next7 =  0  0  0  0  0  0  0 d0 | 0  0
                    prev = _mm_add_epi16(prev0, prev);
                    next = _mm_add_epi16(next, next7);
                    __m128i cur2 = _mm_add_epi16(cur, cur);
                    __m128i sum = _mm_add_epi16(prev, next);
                    sum = _mm_add_epi16(sum, cur2);

                    // Advance
                    prev0 = _mm_bsrli_si128(cur, 14);
                    prein = in;

                    // Store 128-bits
                    _mm_store_si128(reinterpret_cast<__m128i*>(output), sum);
                    output += VEC_LEN / 2;
                }
            }

            if (i < w) {
                assert(false); // Not implemented
            }
        }
    }

    inline void sobelYVLine(
        const int16_t* prevLine, const int16_t* nextLine, int16_t* output, int w)
    {
        constexpr size_t VEC_LEN = 8;

        const __m128i zero = _mm_setzero_si128();

        int i = 0;
        for (; i + VEC_LEN <= w; i += VEC_LEN) {
            // Load 128-bits of input
            const __m128i prev = _mm_load_si128(reinterpret_cast<const __m128i*>(prevLine));
            prevLine += VEC_LEN;
            const __m128i next = _mm_load_si128(reinterpret_cast<const __m128i*>(nextLine));
            nextLine += VEC_LEN;

            // prev - next
            __m128i diff = _mm_sub_epi16(prev, next);

            // Store 128-bits
            _mm_store_si128(reinterpret_cast<__m128i*>(output), diff);
            output += VEC_LEN;
        }

        for (; i < w; i++) {
            const int16_t diff = *prevLine++ - *nextLine++;
            *output++ = diff;
        }
    }

    void sobelYV(const int16_t* input, int16_t* output, size_t w, size_t h)
    {
        const int16_t* prevLine = input;
        const int16_t* currentLine = input;

        for (size_t y = 0; y < h - 1; ++y) {
            const int16_t* nextLine = currentLine + w;
            sobelYVLine(prevLine, nextLine, output, w);
            output += w;
            prevLine = currentLine;
            currentLine = nextLine;
        }

        sobelYVLine(prevLine, currentLine, output, w);
    }

    void calculateSobel(const uint8_t* input, int16_t* dx, int16_t* dy, size_t w, size_t h)
    {
        std::vector<int16_t> temp(w * h);
        sobelXH(input, &temp[0], w, h);
        sobelXV(&temp[0], dx, w, h);
        sobelYH(input, &temp[0], w, h);
        sobelYV(&temp[0], dy, w, h);
    }

    // sobel max 8 * 255 -> 11bits, squared -> 22-bits, sum -> 23-bits
    void calculateMagnitude(size_t size, const int16_t* dx, const int16_t* dy, int32_t* mag)
    {
        // Unrolled x2, because there are at least two arithmetic units
        constexpr size_t VEC_LEN = 16 / 2;

        __m128i max1 = _mm_setzero_si128();
        __m128i max2 = max1;

        int i = 0;
        for (; i + VEC_LEN <= size; i += VEC_LEN) {
            // Load 128-bits of dx and dy
            __m128i dxd = _mm_load_si128(reinterpret_cast<const __m128i*>(dx));
            __m128i dyd = _mm_load_si128(reinterpret_cast<const __m128i*>(dy));
            dx += VEC_LEN;
            dy += VEC_LEN;

            // Interleave
            __m128i dxy_lo = _mm_unpacklo_epi16(dxd, dyd);
            __m128i dxy_hi = _mm_unpackhi_epi16(dxd, dyd);

            // Square and sum pairs -> store 32-bit values
            __m128i res_lo = _mm_madd_epi16(dxy_lo, dxy_lo);
            __m128i res_hi = _mm_madd_epi16(dxy_hi, dxy_hi);

            // Store 128-bits (total of 8 32-bit ints)
            _mm_store_si128(reinterpret_cast<__m128i*>(mag), res_lo);
            _mm_store_si128(reinterpret_cast<__m128i*>(mag + 4), res_hi);
            mag += VEC_LEN;
        }

        // Do the rest old fashion way
        for (; i < size; ++i) {
            *mag++ = *dx * *dx + *dy * *dy;
            dx++;
            dy++;
        }
    }

    void calculateGradient(const ImageBuffer<uint8_t>& input, ImageBuffer<int16_t>& Gx, ImageBuffer<int16_t>& Gy,
        ImageBuffer<int>& G)
    {
        // Make sobel convolutions
        calculateSobel(input.getRawData(), Gx.getRawData(), Gy.getRawData(), input.getWidth(), input.getHeight());
#ifndef NDEBUG
        const int w = input.getWidth();
        const int h = input.getHeight();
        ImageBuffer<int16_t> sx(w, h), sy(w, h);
        rock::calculateSobel(input, sx, sy);

        for (size_t y = 0; y < h; ++y) {
            for (size_t x = 0; x < w; ++x) {
                if (Gx.get(x, y) != sx.get(x, y)) __debugbreak();
            }
        }
        for (size_t y = 0; y < h; ++y) {
            for (size_t x = 0; x < w; ++x) {
                if (Gy.get(x, y) != sy.get(x, y)) __debugbreak();
            }
        }
#endif

        const auto* dx = Gx.getRawData();
        const auto* dy = Gy.getRawData();
        auto* mag = G.getRawData();
        calculateMagnitude(input.getPixelCount(), dx, dy, mag);
#ifndef NDEBUG
        std::vector<int> test(input.getPixelCount());
        for (size_t i = 0; i < input.getPixelCount(); ++i) {
            test[i] = dx[i] * dx[i] + dy[i] * dy[i];
        }

        assert(!memcmp(&test[0], mag, input.getPixelCount() * 4));
#endif
    }

    const uint8_t STRONG = 255;
    const uint8_t WEAK = 144;

    int maximum(size_t size, const int* data)
    {
        // Unrolled x2, because there are at least two arithmetic units
        constexpr size_t VEC_LEN = 32 / 4;

        __m128i max1 = _mm_setzero_si128();
        __m128i max2 = max1;

        int i = 0;
        for (; i + VEC_LEN <= size; i += VEC_LEN) {
            // Load 128-bits x 2
            __m128i in1 = _mm_load_si128(reinterpret_cast<const __m128i*>(data));
            __m128i in2 = _mm_load_si128(reinterpret_cast<const __m128i*>(data + 4));
            data += VEC_LEN;

            // Calculate maximums
            max1 = _mm_max_epi32(in1, max1);
            max2 = _mm_max_epi32(in2, max2);
        }

        // Get result from SIMD operations
        alignas(16) int maxValues[VEC_LEN];
        _mm_store_si128(reinterpret_cast<__m128i*>(&maxValues[0]), max1);
        _mm_store_si128(reinterpret_cast<__m128i*>(&maxValues[VEC_LEN / 2]), max2);

        int result = 0;
        for (size_t j = 0; j < std::size(maxValues); ++j) {
            result = std::max(maxValues[j], result);
        }

        // Do the rest old fashion way
        for (; i < size; ++i) {
            result = std::max(*data++, result);
        }

        return result;
    }

    void removeWeak(size_t size, uint8_t* data)
    {
        // Unrolled x2, because there are at least two arithmetic units
        constexpr size_t VEC_LEN = 32;

        const __m128i strongs = _mm_setr_epi8(
            STRONG, STRONG, STRONG, STRONG, STRONG, STRONG, STRONG, STRONG,
            STRONG, STRONG, STRONG, STRONG, STRONG, STRONG, STRONG, STRONG);

        int i = 0;
        for (; i + VEC_LEN <= size; i += VEC_LEN) {
            // Load 128-bits x 2
            __m128i in1 = _mm_load_si128(reinterpret_cast<const __m128i*>(data));
            __m128i in2 = _mm_load_si128(reinterpret_cast<const __m128i*>(data + 16));

            // Compare to strong. If equal returns 0xff (i.e. strong) and 0 otherwise (removes weak)
            __m128i res1 = _mm_cmpeq_epi8(in1, strongs);
            __m128i res2 = _mm_cmpeq_epi8(in2, strongs);

            // Store 128-bits x 2
            _mm_store_si128(reinterpret_cast<__m128i*>(data), res1);
            _mm_store_si128(reinterpret_cast<__m128i*>(data + 16), res2);
            data += 32;
        }

        for (; i < size; ++i) {
            if (data[i] == WEAK) {
                data[i] = 0;
            }
        }
    }
}

using namespace paper;

void executePaper(const Image& original, ImageBuffer<uint8_t>& result)
{
    const auto w = original.getWidth();
    const auto h = original.getHeight();
    double t1 = get_elapsed_mcycles();
    ImageBuffer<uint8_t> data(w, h);
    convertToGrayScale(original, data);
    double t2 = get_elapsed_mcycles();
    e12 = std::min(e12, t2 - t1);

    gaussianBlur(data.getData(), w, h);
    double t3 = get_elapsed_mcycles();
    e23 = std::min(e23, t3 - t2);

    ImageBuffer<int> G(w, h);
    ImageBuffer<int16_t> Gx(w, h);
    ImageBuffer<int16_t> Gy(w, h);
    calculateGradient(data, Gx, Gy, G);
    double t4 = get_elapsed_mcycles();
    e34 = std::min(e34, t4 - t3);

    std::stack<std::pair<int, int>> strong;
    const auto Gmax = maximum(G.getPixelCount(), G.getRawData());
    rock::nonMaxSuppressionAndThreshold2(
        G, Gx, Gy, result, Gmax, 0.3f, 0.275f, strong);
    double t5 = get_elapsed_mcycles();
    e45 = std::min(e45, t5 - t4);

    rock::hysteresis(result, strong);
    removeWeak(result.getPixelCount(), result.getRawData());
    double t6 = get_elapsed_mcycles();
    e56 = std::min(e56, t6 - t5);
}
