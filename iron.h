#pragma once

#include "shared.h"

namespace iron {
    void convertToGrayScale(const Image& input, ImageBuffer<uint8_t>& output);
    void gaussBlur_4_s14(std::vector<unsigned char>& data, size_t w, size_t h);

    void boxBlurH_4_r1(const std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h);
    void boxBlurV_4_r1(const std::vector<unsigned char>& scl, std::vector<unsigned char>& tcl, size_t w, size_t h);
}
